### Pre requisits
- node 12.16

### Run/ Build
npx cap add android

# build
npm run build

npx cap copy

optional if plugins were added/removed: npx cap update

npx cap open android


# Run Tests

* All tests
ng test

* or searching in a specific folder
ng test --include='src/tests'

* only state tests
ng test --include='src/tests/state'

* only services tests
ng test --include='src/tests/services'

* A specific test
ng test --include='src/tests/state/panel-state.spec.ts'

# ERRORS
### can't use android.support.v4.content.FileProvider
Find FileProvider.java and replace android.support.v4.content.FileProvider by androidx.core.content.FileProvider

### can't sync for ios
- see https://github.com/ionic-team/capacitor/issues/1448

### Crashlytics could not find the manifest
ref: https://stackoverflow.com/questions/54490247/crashlytics-could-not-find-the-manifest-when-update-buildgradle-to-3-3-0/61281829#61281829

- COPY: android/capacitor-cordova-android-plugins/src/main/AndroidManifest.xml
- to: android/capacitor-cordova-android-plugins/build/intermediates/merged_manifests/debug/AndroidManifest.xml


### resource color/accent (aka <your_package>/color/accent) not found
- adapted from: https://stackoverflow.com/questions/58668110/ionic-4-fcm-how-to-fix-the-compiler-error-coloraccent-not-found
- Create the file colors.xml in app/res/values/ with at least this content

`<?xml version="1.0" encoding="utf-8"?>
<resources>
    <color name="accent">#any_color</color>
</resources>`


### common.util.concurrent.ListenableFuture found in modules jetified-guava-26.0-android.jar (com.google.guava:guava:26.0-android) and jetified-listenablefuture-1.0.jar (com.google.guava:listenablefuture:1.0)

- ref: https://stackoverflow.com/questions/56639529/duplicate-class-com-google-common-util-concurrent-listenablefuture-found-in-modu
- Add this line in build.gradle of your module
implementation 'com.google.guava:guava:27.0.1-android'

### The Crashlytics build ID is missing. This occurs when Crashlytics tooling is absent from your app's build configuration
- ref: https://stackoverflow.com/a/58486547
- From: https://stackoverflow.com/questions/54706847/fabric-debug-craslytic-reports-signup-build-id-missing-apply-plugin-io-fab

- In build.gradle (app) set

apply plugin: 'com.android.application'
apply plugin: 'io.fabric'
...
- In build.gradle (android) set

buildscript{
  repositories{
    ...
    maven {
      url 'https://maven.fabric.io/public'
    }
  }
  dependencies{
    ...
    classpath 'io.fabric.tools:gradle:1.31.2'
  }
}

### [warn] Plugin cordova-plugin-firebasex requires you to add 
  <key>UIBackgroundModes</key>
  <array>
    <string>remote-notification</string>
  </array>

### Cannot open Whats app on iOS
- add entry in info.plist: LSApplicationQueriesSchemes <array> = item0 <string> = whatsapp