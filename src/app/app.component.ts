import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService, LoginStore, TOKEN_KEY } from '@boxx/login-core';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Platform, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import 'moment/locale/es';
import { APP_CONSTANTS } from 'src/constants/constants';
import { environment } from 'src/environments/environment';
import { LocalStorageNativeService } from 'src/services/NativeLocalStorage.service';
import { SharingService } from 'src/services/NativeSharing.service';

const LOCALE_LANG =
    'en';
// 'en';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent {

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private translateService: TranslateService,
        private storage: LocalStorageNativeService,
        private router: Router,
        private authSrvc: LoginService,
        private loginStore: LoginStore,
        private sharingSrvc: SharingService,
        public toastController: ToastController,
        private appVersion: AppVersion
    ) {
        (window as any).skipLocalNotificationReady = true;
        this.initializeApp();

        this.checkEnvironment();
    }

    isIos = false;
    isCordovaOrCApacitor = false;

    public appPages = [{
        title: 'PROFILE.title',
        url: '/profile/detail',
        icon: 'person-circle-outline'
    },
    {
        title: 'PROFILE.faqs',
        url: '/profile/faqs',
        icon: 'chatbox-ellipses-outline'
    },
    {
        title: 'PROFILE.support',
        url: '/profile/support',
        icon: 'headset-outline'
    },
    {
        title: 'PROFILE.blog',
        url: false,
        icon: 'card-outline',
        external: APP_CONSTANTS.BLOG_URL
    }];

    version: string;

    initializeApp() {
        this.platform.ready().then(() => {
            this.appVersion.getVersionNumber().then(vnumber => this.version = vnumber);

            this.router.navigateByUrl('/', { skipLocationChange: true, replaceUrl: true });

            this.isIos = this.platform.is('ios');
            this.isCordovaOrCApacitor = this.platform.is('cordova') || this.platform.is('capacitor');

            this.statusBar.styleDefault();
            this.splashScreen.hide();
            this.translateService.setDefaultLang(LOCALE_LANG);
            this.translateService.use(LOCALE_LANG);
            moment.locale(LOCALE_LANG);

            this.storage.getItem(TOKEN_KEY).then(tokenKey => {
                if (tokenKey) {
                    this.authSrvc.getSession().then(User => {
                        setTimeout(() => {
                            this.loginStore.setUserData(User);
                            this.router.navigateByUrl('tabs', { skipLocationChange: true, replaceUrl: true });
                        }, 900);
                    });
                }
                else {
                    this.router.navigateByUrl('login', { skipLocationChange: true, replaceUrl: true });
                }
            });
        });
    }

    openPrivacyPolicy() {
        // let url = (!this.isIos ? 'https://docs.google.com/viewer?url=' : '') +
        this.sharingSrvc.openURL(APP_CONSTANTS.PRIVACY_POLICY_URL);
    }

    async checkEnvironment() {
        if (environment.production) { return; }

        const toast = await this.toastController.create({
            message: 'La aplicación se está ejecutando en modo desarrollo',
            color: 'warning',
            buttons: [
                {
                    side: 'end',
                    icon: 'close-outline',
                    // text: 'Favorite',
                    handler: () => { }
                }
            ]
        });
        toast.present();
    }

    openExternalUrl(url: string) {
        this.sharingSrvc.openURL(url);
    }
}
