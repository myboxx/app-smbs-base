import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { JwtModule, JWT_OPTIONS } from '@auth0/angular-jwt';
import { ContactsCoreModule } from '@boxx/contacts-core';
import { EventsCoreModule } from '@boxx/events-core';
import {
    jwtOptionsFactory,
    LoginCoreModule,
    NATIVE_STORAGE_SERVICE
} from '@boxx/login-core';
import { MessagesCoreModule } from '@boxx/messages-core';
import { PanelCoreModule } from '@boxx/panel-core';
import { ProfileCoreModule } from '@boxx/profile-core';
import { LOCAL_NOTIFICATIONS_SERVICE, SharedModulesModule } from '@boxx/shared-modules';
import { WebsiteCoreModule } from '@boxx/website-core';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Device } from '@ionic-native/device/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { APP_CONSTANTS } from 'src/constants/constants';
import { environment } from 'src/environments/environment';
import { containerService } from 'src/services/container';
import { LocalNotificationsService } from 'src/services/NativeLocalNotification.service';
import { LocalStorageNativeService } from 'src/services/NativeLocalStorage.service';
import { ServicesModule } from 'src/services/services.module';
import { MultilingualModule } from 'src/services/translate/translate.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ErrorInterceptor } from './helpers/error-interceptor';
import { JwtInterceptor } from './helpers/jwt-interceptor';

@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [
        BrowserModule,
        IonicModule.forRoot({ backButtonText: '', backButtonIcon: 'chevron-back-outline' }),
        AppRoutingModule,
        HttpClientModule,
        JwtModule.forRoot({
            config: {
                throwNoTokenError: true
                // whitelistedDomains: ["..."],
                // blacklistedRoutes: ["..."]
            },
            jwtOptionsProvider: {
                provide: JWT_OPTIONS,
                useFactory: jwtOptionsFactory,
                deps: [LocalStorageNativeService]
            }
        }),

        StoreModule.forRoot({}),
        EffectsModule.forRoot([]),

        ContactsCoreModule.forRoot({
            apiUrl: environment.apiUrl,
            instanceName: environment.instanceName
        }),

        MessagesCoreModule.forRoot({
            apiUrl: environment.apiUrl,
            instanceName: environment.instanceName
        }),

        EventsCoreModule.forRoot({
            apiUrl: environment.apiUrl,
            instanceName: environment.instanceName
        }),

        WebsiteCoreModule.forRoot({
            apiUrl: environment.apiUrl,
            instanceName: environment.instanceName
        }),

        LoginCoreModule.forRoot({
            apiUrl: environment.apiUrl,
            instanceName: environment.instanceName,
            loginOptionsProvider: {
                provide: NATIVE_STORAGE_SERVICE,
                useClass: LocalStorageNativeService
            }
        }),

        PanelCoreModule.forRoot({
            apiUrl: environment.apiUrl,
            instanceName: environment.instanceName
        }),

        ProfileCoreModule.forRoot({
            apiUrl: environment.apiUrl,
            instanceName: environment.instanceName
        }),

        SharedModulesModule.forRoot({
            appConstants: APP_CONSTANTS,
            sharedModulesOptionsProvider: {
                provide: LOCAL_NOTIFICATIONS_SERVICE,
                useClass: LocalNotificationsService
            }
        }),

        StoreDevtoolsModule.instrument({
            name: 'Virket core (ionic5 App) :)',
            logOnly: environment.production,
            maxAge: 25
        }),

        MultilingualModule,
        ServicesModule
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
        StatusBar,
        SplashScreen,
        NativeStorage,
        LocalNotifications,
        Device,
        AppVersion,
        ...containerService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
