import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LoginService } from '@boxx/login-core';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private authenticationService: LoginService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            catchError(err => {
                console.error('--- INTERCEPTING ERROR IN HTTP REQUEST', request.url);
                if (err.status === 401) {
                    console.error('--- A 401 response was received for', err.url);

                    // if not from login request
                    if (request.url.indexOf('auth/login') === -1) {
                        // auto logout if 401 response returned from api
                        console.log('--- AUTO LOGOUT AND RELOADING PAGE!!!');

                        this.authenticationService.logout().subscribe(() => {
                            location.reload(true);
                        });
                    }
                }

                const error = err.error.message || err.statusText;
                console.error('--- ERROR::', error);

                return throwError(error);
            }));
    }
}
