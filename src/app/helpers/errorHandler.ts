import { Injectable } from '@angular/core';
import { IHttpBasicResponse } from '@boxx/core';

@Injectable()
export class RepositoryErrorHandler implements IHttpErrorHandler{
    handle(response: IHttpBasicResponse<any>) {

        if (response.statusCode && response.statusCode >= 400 && response.statusCode < 500){
            throw this.buildErrorResponse(response.error || response.message || response.statusText, response.statusCode);
        }
        else if (response.statusCode && response.statusCode >= 500 && response.statusCode < 600){
            throw this.buildErrorResponse(response.error || response.message || response.statusText, response.statusCode);
        }
        else if (response.status !== 'success') {
            throw this.buildErrorResponse(response.error || response.message || response.statusText, undefined);
        }

        return response;
    }

    private buildErrorResponse(error: any = 'UNKNOWN ERROR', code: number){
        // location.reload()
        // console.error(`RepositoryErrorHandler.handle(error ${code})`, error);
        return {error: `[ERROR HANDLER] ${error.message ? error.message : error}, Error Code: ${code}`};
    }
}

export interface IHttpErrorHandler {
    handle(response: any): any;
}
