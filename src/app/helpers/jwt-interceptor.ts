import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TOKEN_KEY } from '@boxx/login-core';
import { from, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { LocalStorageNativeService } from 'src/services/NativeLocalStorage.service';

@Injectable({providedIn: 'root'})
export class JwtInterceptor implements HttpInterceptor {
    constructor(private localStge: LocalStorageNativeService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return from(
            this.localStge.getItem(TOKEN_KEY)).pipe(
            switchMap(token => {
                if (token && (request.url.indexOf('auth/login') === -1 && request.url.indexOf('restcountries') === -1)) {
                    request = request.clone({ headers: request.headers.set('Authorization', `Bearer ${token}`) });
                }

                if (!request.headers.has('Content-Type')) {
                    request = request.clone({ headers: request.headers.set('Content-Type', 'application/x-www-form-urlencoded') });
                }

                return next.handle(request);
            })
        );
    }
}
