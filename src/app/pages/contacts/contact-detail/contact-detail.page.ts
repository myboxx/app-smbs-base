import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
    ContactInteractionModel,
    ContactModel,
    ContactStore,
    CONTACT_TYPE
} from '@boxx/contacts-core';
import { ContactUpsertModalComponent, EventUpsertModalComponent } from '@boxx/shared-modules';
import {
    ActionSheetController,
    AlertController,
    LoadingController,
    ModalController,
    NavController,
    Platform,
    ToastController
} from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { Observable, Subject } from 'rxjs';
import { takeUntil, withLatestFrom } from 'rxjs/operators';
import { FirebasexService } from 'src/services/NativeFirebaseX.service';
import { SharingService } from 'src/services/NativeSharing.service';

@Component({
    selector: 'app-contact-detail',
    templateUrl: './contact-detail.page.html',
    styleUrls: ['./contact-detail.page.scss'],
})
export class ContactDetailPage implements OnInit, OnDestroy {

    constructor(
        private route: ActivatedRoute,
        private store: ContactStore,
        private modalCtrl: ModalController,
        public actionSheetCtrl: ActionSheetController,
        private alertCtrl: AlertController,
        public loadingCtrl: LoadingController,
        private navCtrl: NavController,
        private translate: TranslateService,
        private sharingSrvc: SharingService,
        public toastController: ToastController,
        private plt: Platform,
        private firebasex: FirebasexService
    ) {
        this.translate.get(['CONTACTS', 'GENERAL'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe((t: any) => this.translations = t);

        this.isIos = !this.plt.is('android');
    }

    loading$: Observable<boolean>;
    loadingInteractions$: Observable<boolean>;
    interactionsError$: Observable<any>;
    destroyed$ = new Subject<boolean>();
    loader: any;

    contact$: Observable<ContactModel>;
    contact: ContactModel = ContactModel.empty();
    interactions$: Observable<Array<ContactInteractionModel>>;

    userType: CONTACT_TYPE = 'NOT_SPECIFIED';

    translations: any = {};

    isIos = true;

    ngOnInit() {
        const contactId = +this.route.snapshot.paramMap.get('id');

        this.contact$ = this.store.ContactById$(contactId);

        this.store.Contacts$
            .pipe(
                takeUntil(this.destroyed$),
                withLatestFrom(this.store.HasBeenFetched$)
            ).subscribe(async ([cl, hasBeenFetched]) => {

                if (!hasBeenFetched) { return; }

                const contact = cl.filter(c => c.id === contactId)[0];

                if (!contact) {
                    if (this.loader) {
                        await this.loadingCtrl.dismiss();
                        this.loader = null;
                    }

                    this.navCtrl.navigateBack('/tabs/contacts', { queryParams: { tab: 'ALL' } });

                    return false;
                }

                this.contact = contact;
                this.userType = contact.type;
            });

        this.loading$ = this.store.Loading$;

        this.loading$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(async (loading) => {

                if (loading) {
                    this.loader = await this.loadingCtrl.create();
                    this.loader.present();
                }
                else {
                    if (this.loader) {
                        this.loadingCtrl.dismiss();
                        this.loader = null;
                    }
                }
            });

        this.store.HasBeenFetched$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(fetched => {
                if (!fetched) {
                    this.store.fetchContacts();
                }
            });

        this.interactions$ = this.store.Interactions$;
        this.loadingInteractions$ = this.store.LoadingInteractions$;
        this.interactionsError$ = this.store.InteractionsError$;
        this.store.fetchInteractions$(contactId);

        this.firebasex.setScreenName('CONTACTS > DETAIL');
    }

    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }

    makeCall() {
        this.sharingSrvc.makeCall(this.contact.phone);
        this.store.createInteraction(this.contact.id, { action_type: 'CONTACT_CALL', entity: 'contacts', entity_id: this.contact.id });
    }

    sendMail() {
        this.sharingSrvc.sendEmail(this.contact.email);
        this.store.createInteraction(this.contact.id, { action_type: 'CONTACT_EMAIL', entity: 'contacts', entity_id: this.contact.id });
    }

    async includeInNewEvent() {
        const modal = await this.modalCtrl.create({
            component: EventUpsertModalComponent,
            componentProps: {
                action: 'CREATE',
                period: { from: moment(), to: moment() },
                attendees: [this.contact]
            }
        });

        this.firebasex.setScreenName('CONTACT > EVENTS > CREATE');

        await modal.present();

        const { data } = await modal.onWillDismiss();

        if (data) {
            this.store.createInteraction(
                this.contact.id, { action_type: 'CONTACT_SCHEDULED', entity: 'schedule', entity_id: data.eventId }
            );
        }
    }

    async sendWhatsapp() {
        if (this.contact.phone.length !== 10) {
            (await this.alertCtrl.create({
                header: this.translations.GENERAL.ACTION.confirm,
                message: this.translations.CONTACTS.confirmUpdateContactPhoneMsg,
                buttons: [{
                    text: this.translations.GENERAL.ACTION.cancel,
                    role: 'cancel',
                }, {
                    text: this.translations.GENERAL.ACTION.ok,
                    handler: () => this.presentModal()
                }]
            })).present();

            return;
        }

        this.sharingSrvc.whatsAppSendMessage(
            this.contact.phone, this.contact.countryCode, this.contact.phoneCode, this.translations.GENERAL.hi
        ).then(
            async () => {
                const toast = await this.toastController.create({
                    message: this.translations.CONTACTS.SUCCESS.actionCompleted,
                    duration: 3000,
                    color: 'success'
                });
                toast.present();

                this.store.createInteraction(this.contact.id, { action_type: 'CONTACT_WHATSAPP', entity: 'contacts', entity_id: 1 });
            },
            async e => {
                const toast = await this.toastController.create({
                    message: `${this.translations.CONTACTS.ERRORS[e.after]} <br> ${e.error}`,
                    duration: 5000,
                    color: 'danger'
                });
                toast.present();
            }
        );
    }

    async showOptions() {
        const actionSheet = await this.actionSheetCtrl.create({
            header: this.translations.GENERAL.contact,
            buttons: [{
                text: this.translations.GENERAL.ACTION.delete,
                role: 'destructive',
                handler: async () => {
                    (await this.alertCtrl.create({
                        header: this.translations.GENERAL.ACTION.confirm,
                        message: this.translations.CONTACTS.deleteConfirmMsg,
                        buttons: [{
                            text: this.translations.GENERAL.ACTION.cancel,
                            role: 'cancel',
                            cssClass: 'primary',
                            // handler: () => { }
                        }, {
                            text: this.translations.GENERAL.ACTION.ok,
                            handler: () => {
                                this.store.deleteContact(this.contact.id);
                            }
                        }]
                    })).present();
                    return true;
                }
            }, {
                text: this.translations.GENERAL.ACTION.edit,
                handler: () => {
                    this.presentModal();
                }
            }, {
                text: this.translations.GENERAL.ACTION.cancel,
                role: 'cancel',
                // handler: () => { }
            }]
        });

        await actionSheet.present();

    }

    async presentModal() {

        const modal = await this.modalCtrl.create({
            component: ContactUpsertModalComponent,
            componentProps: {
                mode: 'UPDATE',
                contact: this.contact
            }
        });

        this.firebasex.setScreenName('CONTACTS > UPDATE');

        await modal.present();

        const { data } = await modal.onWillDismiss();
    }
}
