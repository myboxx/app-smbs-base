import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import {
    ContactModel,
    ContactStore,
    CONTACTS_SERVICE,
    IContactsService
} from '@boxx/contacts-core';
import { ContactUpsertModalComponent } from '@boxx/shared-modules';
import { Contact } from '@ionic-native/contacts';
import { LoadingController, ModalController, NavController } from '@ionic/angular';
import { Observable, Subject } from 'rxjs';
import { takeUntil, withLatestFrom } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { FirebasexService } from 'src/services/NativeFirebaseX.service';

@Component({
    selector: 'app-contact-import',
    templateUrl: './contact-import.page.html',
    styleUrls: ['./contact-import.page.scss'],
})
export class ContactImportPage implements OnInit, OnDestroy {

    constructor(
        @Inject(CONTACTS_SERVICE) private contactsService: IContactsService<Contact, ContactModel>,
        private store: ContactStore,
        public loadingCtrl: LoadingController,
        private navCtrl: NavController,
        private modalCtrl: ModalController,
        private firebasex: FirebasexService
    ) {
        this.isLoading = true;
        this.contactsService.loadFormattedNativeContacts()
            .pipe(withLatestFrom(this.store.Contacts$))
            .subscribe(([nativeList, storedList]) => {

                const uniqueList = nativeList.filter(c1 => {
                    return !storedList.some(c2 => {
                        return c2.phone === c1.phone;
                    });
                });

                this.contactList = uniqueList.map(c => ({ isChecked: false, contact: c }));
                this.isLoading = false;

            }, error => {
                console.error('loadFormattedNativeContacts', error);

                if (environment.production) { return; }

                setTimeout(() => {
                    this.isLoading = false;

                    this.contactList = [
                        {
                            isChecked: false, contact: new ContactModel({
                                id: null,
                                name: 'Prueba',
                                lastName: 'Fake',
                                email: 'abc@uno.com',
                                type: 'CLIENT',
                                phone: '+52 (456) 123 12-34',
                                streetAddress: 'abc1234',
                                city: null
                            })
                        },
                        {
                            isChecked: false, contact: new ContactModel({
                                id: null,
                                name: 'Prueba 2',
                                lastName: 'Fake 2',
                                email: 'abcd@uno.com',
                                type: 'PROSPECT',
                                phone: '123',
                                streetAddress: 'abc123',
                                city: ''
                            })
                        },
                        {
                            isChecked: false, contact: new ContactModel({
                                id: null,
                                name: 'Prueba 3',
                                lastName: 'Fake 3',
                                email: 'abcde@uno.com',
                                type: 'NOT_SPECIFIED',
                                phone: '987',
                                streetAddress: 'abc1235',
                                city: null
                            })
                        },
                    ];
                }, 1000);
            });
    }

    contactList: Array<{ isChecked: boolean, contact: ContactModel }> = [];

    isIndeterminate: boolean;
    masterCheck: boolean;

    loading$: Observable<boolean>; // <--for back drop loader
    destroyed$ = new Subject<boolean>();
    loader: any;
    isLoading = false; // <--for showing skeleton html
    isImporting = false;

    ngOnInit() {
        this.loading$ = this.store.Loading$;

        this.loading$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(async (loading) => {

                if (loading) {
                    this.loader = await this.loadingCtrl.create();
                    this.loader.present();
                }
                else {
                    if (this.loader) {
                        this.loadingCtrl.dismiss();
                        this.loader = null;
                    }
                }

                if (this.isImporting) {
                    this.isImporting = false;
                    setTimeout(() => {
                        this.navCtrl.navigateBack('/tabs/contacts', { queryParams: { tab: 'ALL' } });
                    }, 800);
                }
            });

        this.firebasex.setScreenName('CONTACTS > IMPORT');
    }

    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }

    hasSelectedContacts() {
        return this.contactList.some(v => v.isChecked === true);
    }

    async importContacts() {

        const selectedContacts = this.contactList.filter(c => c.isChecked)
            .map(cl => cl.contact);

        if (selectedContacts.length === 1) {
            const modal = await this.modalCtrl.create({
                component: ContactUpsertModalComponent,
                componentProps: {
                    mode: 'CREATE',
                    contact: selectedContacts[0]
                }
            });

            this.isImporting = true;

            this.firebasex.setScreenName('CONTACTS > IMPORT');

            await modal.present();

            const { data } = await modal.onWillDismiss();
            // The contact creation is triggered inside this^ modal
            // No more actions needed :)
        }
        else {
            this.isImporting = true;
            this.store.importContacts(
                this.contactList
                    .filter(c => c.isChecked)
                    .map(cl => ContactModel.toApiModel(cl.contact, ['phone_code']/*<-- exclude this fields*/))
            );
        }
    }


    checkMaster() {
        setTimeout(() => {
            this.contactList.forEach(obj => {
                obj.isChecked = this.masterCheck;
            });
        });
    }

    checkEvent() {
        const totalItems = this.contactList.length;
        let checked = 0;
        this.contactList.map(obj => {
            if (obj.isChecked) { checked++; }
        });
        if (checked > 0 && checked < totalItems) {
            // If even one item is checked but not all
            this.isIndeterminate = true;
            this.masterCheck = false;
        } else if (checked === totalItems) {
            // If all are checked
            this.masterCheck = true;
            this.isIndeterminate = false;
        } else {
            // If none is checked
            this.isIndeterminate = false;
            this.masterCheck = false;
        }
    }
}
