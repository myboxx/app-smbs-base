import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactDetailPage } from './contact-detail/contact-detail.page';
import { ContactImportPage } from './contact-import/contact-import.page';
import { ContactsPage } from './contacts.page';

const routes: Routes = [
    {
        path: '',
        component: ContactsPage
    },
    {
        path: 'detail/:id',
        component: ContactDetailPage
    },
    {
        path: 'import',
        component: ContactImportPage
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ContactsPageRoutingModule { }
