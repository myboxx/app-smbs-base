import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { ContactDetailPage } from './contact-detail/contact-detail.page';
import { ContactImportPage } from './contact-import/contact-import.page';
import { ContactsPageRoutingModule } from './contacts-routing.module';
import { ContactsPage } from './contacts.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ContactsPageRoutingModule,
        ReactiveFormsModule,
        TranslateModule.forChild(),
        PipesModule
    ],
    declarations: [
        ContactsPage,
        ContactImportPage,
        ContactDetailPage
    ]
})
export class ContactsPageModule { }
