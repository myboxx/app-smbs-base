import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ContactModel, ContactStore, CONTACT_TYPE } from '@boxx/contacts-core';
import { ContactUpsertModalComponent } from '@boxx/shared-modules';
import {
    ActionSheetController,
    AlertController,
    IonSearchbar,
    ModalController,
    Platform,
    ToastController
} from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FirebasexService } from 'src/services/NativeFirebaseX.service';
import { SharingService } from 'src/services/NativeSharing.service';

@Component({
    selector: 'app-contacts',
    templateUrl: './contacts.page.html',
    styleUrls: ['./contacts.page.scss'],
})
export class ContactsPage implements OnInit, OnDestroy {
    @ViewChild('contactsSB', { static: false }) searchBar: IonSearchbar;

    constructor(
        private store: ContactStore,
        private router: Router,
        private actionSheetCtrl: ActionSheetController,
        private translate: TranslateService,
        private shareSrvc: SharingService,
        private modalCtrl: ModalController,
        private alertCtrl: AlertController,
        public toastController: ToastController,
        private plt: Platform,
        private route: ActivatedRoute,
        private firebasex: FirebasexService
    ) {
        this.isIos = !this.plt.is('android');
        this.route.queryParams.subscribe(params => {
            if (params && params.tab) {
                this.filterType = params.tab;
            }
        });
    }

    showSearchBar = false;
    searchValue = '';

    filteredContacts$: Observable<ContactModel[]>;
    isLoading$: Observable<boolean>;
    destroyed$ = new Subject<boolean>();

    loadedContacts: ContactModel[];
    searchResults: ContactModel[] = [];

    translations: any = {};

    filterType: CONTACT_TYPE = 'ALL';

    contactTypes = {
        PROSPECT: 'Prospect',
        CLIENT: 'Clienet',
        NOT_SPECIFIED: 'Not Qualified'
    };

    isIos = true;


    ngOnInit() {
        this.filteredContacts$ = this.store.FilteredContacts$;

        this.isLoading$ = this.store.Loading$;

        this.filteredContacts$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(cList => {
                this.searchResults = cList;
                this.loadedContacts = cList;
            });

        this.store.Contacts$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(() => this.filterBy(this.filterType));

        if (!this.loadedContacts.length) {
            this.fetchContacts();
        }

        this.translate.get(['CONTACTS', 'GENERAL'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe((t: any) => {
                this.translations = t;
                this.contactTypes.PROSPECT = t.GENERAL.prospect;
                this.contactTypes.CLIENT = t.GENERAL.client;
                this.contactTypes.NOT_SPECIFIED = t.GENERAL.not_specified;
            });

        this._handleError();

        this._handleSuccess();
    }

    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }

    ionViewDidEnter() {
        this.firebasex.setScreenName('CONTACTS');
    }

    private _handleSuccess() {
        this.store.Success$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(async (success) => {
                if (success && success.after !== 'GET') {
                    const toast = await this.toastController.create({
                        message: this.translations.CONTACTS.SUCCESS[success.after.toLowerCase()],
                        duration: 3000,
                        color: 'success'
                    });
                    toast.present();
                }
            });
    }

    private _handleError() {
        this.store.Error$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(async (e) => {
                if (e) {
                    const toast = await this.toastController.create({
                        message: this.translations.CONTACTS.ERRORS[e.after.toLowerCase()],
                        duration: 5000,
                        color: 'danger'
                    });
                    toast.present();
                }
            });
    }

    fetchContacts() {
        this.store.fetchContacts();
    }

    filterBy(criteria: CONTACT_TYPE = this.filterType) {
        this.searchValue = ' ';
        setTimeout(() => {
            this.searchValue = '';
        }, 10);

        this.showSearchBar = false;
        this.store.filterContacts(criteria);

    }

    toggleSearch() {
        this.searchValue = ' ';
        setTimeout(() => {
            this.searchValue = '';
        }, 10);
        this.showSearchBar = !this.showSearchBar;

        if (this.showSearchBar) {
            this.searchBar.setFocus();
        }
    }

    async showAddContactOpts() {
        (await this.actionSheetCtrl.create({
            header: this.translations.GENERAL.contact,
            buttons: [{
                text: this.translations.CONTACTS.importFromAgenda,
                // icon: 'download-outline',
                handler: () => {
                    this.router.navigateByUrl('/tabs/contacts/import');
                }
            }, {
                text: this.translations.CONTACTS.createOne,
                // icon: 'create',
                handler: () => {
                    this.presentModal();
                }
            }, {
                text: this.translations.GENERAL.ACTION.cancel,
                // icon: 'close',
                role: 'cancel',
                handler: () => { }
            }]
        })).present();
    }

    async confirmDelete(id: number, slidingItem: any) {
        slidingItem.close();

        (await this.alertCtrl.create({
            header: this.translations.GENERAL.ACTION.confirm,
            message: this.translations.CONTACTS.deleteConfirmMsg,
            buttons: [{
                text: this.translations.GENERAL.ACTION.cancel,
                role: 'cancel',
                cssClass: 'primary',
                // handler: () => { }
            }, {
                text: this.translations.GENERAL.ACTION.ok,
                handler: () => {
                    this.store.deleteContact(id);
                }
            }]
        })).present();
    }

    async openEditorModal(contact: ContactModel, slidingItem: any) {
        slidingItem.close();

        const modal = await this.modalCtrl.create({
            component: ContactUpsertModalComponent,
            componentProps: {
                mode: 'UPDATE',
                contact
            }
        });

        this.firebasex.setScreenName('CONTACTS > UPDATE');

        await modal.present();

        const { data } = await modal.onWillDismiss();
    }

    makeCall(phoneNumber: string) {
        this.shareSrvc.makeCall(phoneNumber);
    }

    sendMail(email: string) {
        this.shareSrvc.sendEmail(email);
    }

    async sendWhatsapp(contact: ContactModel) {
        if (contact.phone.length !== 10) {
            (await this.alertCtrl.create({
                header: this.translations.GENERAL.ACTION.confirm,
                message: this.translations.CONTACTS.confirmUpdateContactPhoneMsg,
                buttons: [{
                    text: this.translations.GENERAL.ACTION.cancel,
                    role: 'cancel',
                }, {
                    text: this.translations.GENERAL.ACTION.ok,
                    handler: () => this.presentModal({ mode: 'UPDATE', contact })
                }]
            })).present();

            return;
        }

        this.shareSrvc.whatsAppSendMessage(contact.phone, contact.countryCode, contact.phoneCode, this.translations.GENERAL.hi)
            .then(
                async () => {
                    const toast = await this.toastController.create({
                        message: this.translations.CONTACTS.SUCCESS.actionCompleted,
                        duration: 3000,
                        color: 'success'
                    });
                    toast.present();
                },
                async e => {
                    const toast = await this.toastController.create({
                        message: `${this.translations.CONTACTS.ERRORS[e.after]} <br> ${e.error}`,
                        duration: 5000,
                        color: 'danger'
                    });
                    toast.present();
                }
            );
    }

    search(searchTerm: string) {

        this.searchResults = this.loadedContacts;

        if (!searchTerm) {
            return;
        }

        this.searchResults = this.searchResults.filter(contact => {
            return contact.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 || contact.phone.indexOf(searchTerm) > -1;
        });
    }

    onCancelSearch() {
        this.showSearchBar = false;
    }

    async presentModal(options: { mode: 'CREATE' | 'UPDATE', contact: ContactModel } = { mode: 'CREATE', contact: ContactModel.empty() }) {
        const modal = await this.modalCtrl.create({
            component: ContactUpsertModalComponent,
            componentProps: {
                mode: options.mode,
                contact: options.contact
            }
        });

        this.firebasex.setScreenName('CONTACTS > ' + options.mode);

        await modal.present();

        const { data } = await modal.onWillDismiss();
        console.log('data preation ===>', data);
        if (options.mode === 'CREATE') {
            this.filterType = 'ALL';
        }
    }

    doRefresh(event) {
        this.fetchContacts();
        setTimeout(() => {
            event.target.complete();
        }, 100);
    }
}
