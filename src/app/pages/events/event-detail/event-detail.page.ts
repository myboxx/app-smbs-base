import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EventModel, EventsStore } from '@boxx/events-core';
import { EventUpsertModalComponent } from '@boxx/shared-modules';
import { ActionSheetController, AlertController, LoadingController, ModalController, NavController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FirebasexService } from 'src/services/NativeFirebaseX.service';
import { LocalNotificationsService } from 'src/services/NativeLocalNotification.service';

@Component({
    selector: 'app-event-detail',
    templateUrl: './event-detail.page.html',
    styleUrls: ['./event-detail.page.scss'],
})
export class EventDetailPage implements OnInit, OnDestroy {

    constructor(
        private route: ActivatedRoute,
        public actionSheetCtrl: ActionSheetController,
        private alertCtrl: AlertController,
        public toastController: ToastController,
        private store: EventsStore,
        public loadingCtrl: LoadingController,
        private navCtrl: NavController,
        private modalCtrl: ModalController,
        private translate: TranslateService,
        private localNotifSrvc: LocalNotificationsService,
        private firebasex: FirebasexService
    ) { }

    event$: Observable<EventModel>;
    destroyed$ = new Subject<boolean>();
    leaved$ = new Subject<boolean>();

    myEvent: EventModel = EventModel.empty();
    loader: any;

    translations: any = {};

    ngOnInit() {
        this.translate.get(['GENERAL', 'EVENTS', 'FORM'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe((t: any) => this.translations = t);

        this.event$ = this.store.EventById$(+this.route.snapshot.paramMap.get('id'));

        this.event$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(ev => { this.myEvent = ev; });

        this._handleError();
        this._handleSuccess();

        this.firebasex.setScreenName('EVENTS > DETAIL');
    }

    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }

    ionViewDidLeave() {
        this.leaved$.next(true);
        this.leaved$.complete();
    }

    private _handleError() {
        this.store.Error$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(async (e) => {
                if (e && e.after === 'DELETE') {
                    const toast = await this.toastController.create({
                        message: this.translations.EVENTS.ERRORS[e.after.toLowerCase()],
                        duration: 5000,
                        color: 'danger'
                    });
                    toast.present();
                }
            });
    }

    private _handleSuccess() {
        this.store.Success$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(async (success) => {
                if (success && success.after === 'DELETE') {
                    const toast = await this.toastController.create({
                        message: this.translations.EVENTS.SUCCESS[success.after.toLowerCase()],
                        duration: 3000,
                        color: 'success'
                    });
                    toast.present();

                    this.localNotifSrvc.removeReminder(+this.route.snapshot.paramMap.get('id'));

                    this.navCtrl.navigateBack('/tabs/events');
                }
            });
    }

    async showEventOptions() {
        const actionSheet = await this.actionSheetCtrl.create({
            header: this.translations.GENERAL.event,
            buttons: [{
                text: this.translations.GENERAL.ACTION.delete,
                role: 'destructive',
                // icon: 'trash',
                handler: async () => {
                    (await this.alertCtrl.create({
                        header: this.translations.GENERAL.ACTION.confirm,
                        message: this.translations.EVENTS.deleteConfirmMsg,
                        buttons: [{
                            text: this.translations.GENERAL.ACTION.cancel,
                            role: 'cancel',
                            cssClass: 'primary',
                            // handler: () => { }
                        }, {
                            text: this.translations.GENERAL.ACTION.ok,
                            handler: () => {
                                this.store.deleteEvent(this.myEvent.id);
                            }
                        }]
                    })).present();
                    return true;
                }
            }, {
                text: this.translations.GENERAL.ACTION.edit,
                // icon: 'create',
                handler: () => {
                    // this.navCtrl.navigateForward('/tabs/events/upsert/update');
                    this.openUpsertModal();
                }
            }, {
                text: this.translations.GENERAL.ACTION.cancel,
                // icon: 'close',
                role: 'cancel',
                // handler: () => { }
            }]
        });

        await actionSheet.present();
    }

    async showAttendeeDetails(contact: { fullName: string, email: string, phone: string }) {
        (await this.alertCtrl.create({
            header: contact.fullName,
            message: `<b>Email</b>: ${contact.email}<br><b>${this.translations.FORM.LABEL.phone}</b>: ${contact.phone}`,
            buttons: [{
                text: this.translations.GENERAL.ACTION.ok,
                role: 'cancel',
                cssClass: 'primary',
                // handler: () => { }
            }]
        })).present();
    }

    async openUpsertModal(period?: { from: any, to: any }) {
        const modal = await this.modalCtrl.create({
            component: EventUpsertModalComponent,
            componentProps: {
                action: 'UPDATE',
                period: period || { from: moment(), to: moment() }
            }
        });

        this.firebasex.setScreenName('EVENTS > UPDATE');

        await modal.present();

        // const { data } = await modal.onWillDismiss();

    }
}
