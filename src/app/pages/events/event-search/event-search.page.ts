import { Component, OnInit, ViewChild } from '@angular/core';
import { EventModel, EventsStore } from '@boxx/events-core';
import { IonSearchbar } from '@ionic/angular';
import * as moment from 'moment';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FirebasexService } from 'src/services/NativeFirebaseX.service';

@Component({
    selector: 'app-event-search',
    templateUrl: './event-search.page.html',
    styleUrls: ['./event-search.page.scss'],
})
export class EventSearchPage implements OnInit {
    @ViewChild('eventsSB', { static: false }) searchBar: IonSearchbar;

    constructor(
        private store: EventsStore,
        private firebasex: FirebasexService
    ) {
    }

    events$: Observable<EventModel[]>;
    destroyed$ = new Subject<boolean>();

    eventList: { [key: string]: { monthName: string, total: number, events: EventModel[] } } = {};

    searchResults: { [key: string]: { monthName: string, total: number, events: EventModel[] } } = {};

    ngOnInit() {

        this.events$ = this.store.Calendar$;

        this.events$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(events => {
                events.forEach(m => {
                    const key = moment(m.datetimeFrom).format('M');

                    const monthName = moment(m.datetimeFrom).format('MMMM').toLocaleUpperCase();

                    if (this.eventList[key]) {
                        this.eventList[key].events.push(m);
                        this.eventList[key].total++;
                    }
                    else {
                        this.eventList[key] = { monthName, total: 1, events: [m] };
                    }
                });

                this.searchResults = this.eventList;
            });

        this.firebasex.setScreenName('EVENTS > SEARCH');
    }

    ionViewDidEnter() {
        this.searchBar.setFocus();
    }

    search(searchTerm: string) {

        this.searchResults = this.eventList;

        if (!searchTerm) {
            return;
        }

        const results: { [key: string]: { monthName: string, total: number, events: EventModel[] } } = {};

        Object.keys(this.searchResults).forEach(key => {
            const res = this.searchResults[key].events.filter(event => {
                return event.title.toLowerCase().indexOf(searchTerm) > -1;
            });

            if (res.length) {
                results[key] = { monthName: this.searchResults[key].monthName, total: res.length, events: res };
            }
        });

        this.searchResults = results;
    }
}
