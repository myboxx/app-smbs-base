import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EventsPage } from './events.page';
import { EventDetailPage } from './event-detail/event-detail.page';
import { EventSearchPage } from './event-search/event-search.page';

const routes: Routes = [
    {
        path: '',
        component: EventsPage
    },
    {
        path: 'detail/:id',
        component: EventDetailPage
    },
    {
        path: 'search',
        component: EventSearchPage
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CalendarPageRoutingModule { }
