import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EventsCoreModule } from '@boxx/events-core';
import { SharedModulesModule } from '@boxx/shared-modules';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { CalendarModule } from 'ion2-calendar';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { EventDetailPage } from './event-detail/event-detail.page';
import { EventSearchPage } from './event-search/event-search.page';
import { CalendarPageRoutingModule } from './events-routing.module';
import { EventsPage } from './events.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        CalendarPageRoutingModule,
        CalendarModule,
        TranslateModule.forChild(),
        EventsCoreModule,
        SharedModulesModule,
        PipesModule,
    ],
    declarations: [EventsPage, EventDetailPage, EventSearchPage]
})
export class EventsPageModule { }
