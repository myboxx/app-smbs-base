import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EventModel, EventsStore } from '@boxx/events-core';
import { EventUpsertModalComponent } from '@boxx/shared-modules';
import { AlertController, ModalController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { CalendarComponentOptions } from 'ion2-calendar';
import * as moment from 'moment';
import { Moment } from 'moment';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FirebasexService } from 'src/services/NativeFirebaseX.service';

@Component({
    selector: 'app-events',
    templateUrl: './events.page.html',
    styleUrls: ['./events.page.scss'],
})
export class EventsPage implements OnInit, OnDestroy {

    constructor(
        private store: EventsStore,
        private translate: TranslateService,
        private router: Router,
        private route: ActivatedRoute,
        public toastController: ToastController,
        private modalCtrl: ModalController,
        private firebasex: FirebasexService,
        private alertCtrl: AlertController
    ) {
        this.translate.get(['GENERAL', 'EVENTS'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe((t: any) => this.translations = t);

        this.route.queryParams
            .pipe(takeUntil(this.destroyed$))
            .subscribe(params => {
                if (this.router.getCurrentNavigation().extras.state) {
                    this.eventDetailedId = ((this.router.getCurrentNavigation().extras.state.eventId) as number);
                }
            });
    }

    totalEventsLabel = '';

    events$: Observable<EventModel[]>;
    isLoading$: Observable<boolean>;
    destroyed$ = new Subject<boolean>();

    // private subscriptions: Subscription[] = [];

    dateRange: { from: string; to: string; };
    type: 'string'; // 'string' | 'js-date' | 'moment' | 'time' | 'object'
    optionsRange: CalendarComponentOptions = {
        pickMode: 'single',
        weekdays: ['D', 'L', 'M', 'Mi', 'J', 'V', 'S']
    };

    translations: any;

    eventDetailedId: number;

    ngOnInit() {
        this.events$ = this.store.Calendar$;
        this.isLoading$ = this.store.Loading$;

        this.events$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(m => {
                this.translate.get(
                    `EVENTS.${m.length > 0 ? 'youHaveXEvents' : 'youDontHaveEvents'}`, { x: m.length, plural: m.length === 1 ? '' : 's' }
                )
                    .pipe(takeUntil(this.destroyed$))
                    .subscribe(t => {
                        this.totalEventsLabel = t;
                    });

                this._setEventMarkers(m);

                if (m.length && this.eventDetailedId) {
                    this.router.navigateByUrl(`/tabs/events/detail/${this.eventDetailedId}`);
                }
            });

        this.store.getEvents();

        this._handleError();
    }

    private _setEventMarkers(m: Array<EventModel>) {
        if (m.length > 0) {
            const evPerDay: { [key: string]: string } = {};

            m.forEach(ev => {
                const evDate = ev.datetimeFrom.substring(0, 10);
                evPerDay[evDate] !== undefined ? evPerDay[evDate] += '·' : evPerDay[evDate] = '·';
            });

            const markers = m.map(ev => {
                return {
                    date: new Date(ev.datetimeFrom),
                    subTitle: evPerDay[ev.datetimeFrom.substring(0, 10)].substring(0, 4)// <-- num of max markers to show
                };
            });

            this.optionsRange = {
                ...this.optionsRange,
                daysConfig: markers
            };
        }
    }

    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }

    ionViewDidEnter() {
        this.firebasex.setScreenName('EVENTS');
    }

    private _handleError() {
        this.store.Error$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(async (e) => {
                if (e && e.after === 'GET') {
                    const toast = await this.toastController.create({
                        message: this.translations.EVENTS.ERRORS[e.after.toLowerCase()],
                        duration: 5000,
                        color: 'danger'
                    });
                    toast.present();
                }
            });
    }

    onChange(val: Moment) {
        // let navigationExtras: NavigationExtras = { state: { period: { from: val.toDate(), to: val.toDate() } } };
        // this.router.navigate(['tabs/events/upsert/create'], navigationExtras);
        this.openUpsertModal({ from: val.toDate(), to: val.toDate() });
    }

    onSelect(val) {
        /*const date = new Date(val.time)
        let navigationExtras: NavigationExtras = { state: { period: { from:date, to: date } } };
        this.router.navigate(['tabs/events/upsert/create'], navigationExtras);*/
    }

    doRefresh(event) {
        this.eventDetailedId = null;

        this.store.getEvents();
        setTimeout(() => {
            event.target.complete();
        }, 100);
    }

    async openUpsertModal(period?: { from: any, to: any }) {
        const modal = await this.modalCtrl.create({
            component: EventUpsertModalComponent,
            componentProps: {
                action: 'CREATE',
                period: period || { from: moment(), to: moment() }
            }
        });

        this.firebasex.setScreenName('EVENTS > CREATE');

        await modal.present();

        // const { data } = await modal.onWillDismiss();
    }

    async confirmDelete(id: number, slidingItem: any) {
        slidingItem.close();

        (await this.alertCtrl.create({
            header: this.translations.GENERAL.ACTION.confirm,
            message: this.translations.EVENTS.deleteConfirmMsg,
            buttons: [{
                text: this.translations.GENERAL.ACTION.cancel,
                role: 'cancel',
                cssClass: 'primary',
                // handler: () => { }
            }, {
                text: this.translations.GENERAL.ACTION.ok,
                handler: () => {
                    this.store.deleteEvent(id);
                }
            }]
        })).present();
    }

    async openEditorModal(id: number, slidingItem) {
        this.store.EventById$(id);

        slidingItem.close();

        const modal = await this.modalCtrl.create({
            component: EventUpsertModalComponent,
            componentProps: {
                action: 'UPDATE',
                period: { from: moment(), to: moment() }
            }
        });

        this.firebasex.setScreenName('EVENTS > UPDATE');

        await modal.present();

        // const { data } = await modal.onWillDismiss();
    }
}
