import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginStore, UserModel } from '@boxx/login-core';
import { Device } from '@ionic-native/device/ngx';
import { AlertController, LoadingController, MenuController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { APP_CONSTANTS } from 'src/constants/constants';
import { environment } from 'src/environments/environment';
import { FirebasexService } from 'src/services/NativeFirebaseX.service';
import { LocalStorageNativeService } from 'src/services/NativeLocalStorage.service';
import { SharingService } from 'src/services/NativeSharing.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit, OnDestroy {

    constructor(
        private store: LoginStore,
        private formBuilder: FormBuilder,
        private router: Router,
        public loadingCtrl: LoadingController,
        private storage: LocalStorageNativeService,
        private menuCtrl: MenuController,
        private alertCtrl: AlertController,
        private translate: TranslateService,
        private sharingSrvc: SharingService,
        private device: Device,
        private firebasex: FirebasexService
    ) {

        setTimeout(() => {
            this.translate.get(['GENERAL'])
                .pipe(takeUntil(this.destroyed$))
                .subscribe((t: any) => this.translations = t);
        }, 3000);

        this.isLoading$ = store.Loading$;

        this.loadingCtrl.create().then(loader => {
            this.loader = loader;

            this.isLoading$
                .pipe(takeUntil(this.destroyed$))
                .subscribe(async (loading) => {
                    if (loading) {
                        this.loader.present();
                    }
                    else {
                        this.loadingCtrl.dismiss();
                    }
                });
        });

        this.user$ = store.User$;

        store.Error$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(async (error) => {
                if (!error) { return; }

                (await this.alertCtrl.create({
                    header: this.translations.GENERAL.error,
                    message: error,
                    buttons: [{
                        text: this.translations.GENERAL.ACTION.ok
                    }]
                })).present();
            });

        this.user$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(user => {
                if (user.id) {
                    this.storage.getItem('tutorial').then(isOK => {
                        if (isOK !== 'OK') {
                            return this.router.navigate(['onboarding']);
                        }

                        this.router.navigateByUrl('tabs', { skipLocationChange: true, replaceUrl: true });
                    });
                }

            });
    }

    user$: Observable<UserModel>;
    error$: Observable<any>;
    isLoading$: Observable<boolean>;
    destroyed$ = new Subject<boolean>();

    loginForm: FormGroup;

    loader: any;

    validationMessages = {
        email: [
            { type: 'required', message: '•El usuario es requerido' },
            { type: 'pattern', message: '•Formato de email incorrecto' }
        ],
        password: [
            { type: 'required', message: 'Este campo es requerido' },
        ],
    };

    translations: any;

    ngOnInit() {
        console.log('---- LOGIN: ngOnInit');

        this.firebasex.setScreenName('LOGIN');

        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.pattern(APP_CONSTANTS.EMAILPATTERN)]],
            password: ['', Validators.required]
        });
    }

    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }

    login() {
        if (this.loginForm.invalid) {
            return;
        }

        const loginForm = {
            user: this.loginForm.get('email').value,
            password: this.loginForm.get('password').value,
            device_id: this.device.uuid,
            device_model: this.device.model
        };

        this.store.login(loginForm);
    }

    logout() {
        this.store.logout();
    }

    passRecovery() {
        this.sharingSrvc.openURL(environment.apiUrl + '/auth/forgot_my_password/');
    }

    openWizard() {
        // this.router.navigateByUrl('wizard', { skipLocationChange: true, replaceUrl: true });
    }
}
