import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContactModel, ContactStore } from '@boxx/contacts-core';
import { MessageModel, MessagesStore } from '@boxx/messages-core';
import { ContactUpsertModalComponent } from '@boxx/shared-modules';
import {
    ActionSheetController,
    LoadingController,
    ModalController,
    ToastController
} from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FirebasexService } from 'src/services/NativeFirebaseX.service';
import { SharingService } from 'src/services/NativeSharing.service';

@Component({
    selector: 'app-message-detail',
    templateUrl: './message-detail.page.html',
    styleUrls: ['./message-detail.page.scss'],
})
export class MessageDetailPage implements OnInit, OnDestroy {

    constructor(
        private route: ActivatedRoute,
        private store: MessagesStore,
        private contactsStore: ContactStore,
        private actionSheetCtrl: ActionSheetController,
        private translate: TranslateService,
        private sharingSrvc: SharingService,
        private modalCtrl: ModalController,
        public loadingCtrl: LoadingController,
        public toastController: ToastController,
        private firebasex: FirebasexService
    ) {

        this.translate.get(['MESSAGES', 'GENERAL'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe((t: any) => {
                this.translations = t;
            });
    }

    contactsLoading$: Observable<boolean>;
    contact$: Observable<ContactModel[]>;
    isContact = false;

    message$: Observable<MessageModel>;
    destroyed$ = new Subject<boolean>();
    leaved$ = new Subject<boolean>();

    loader: any;
    message: MessageModel = MessageModel.empty();
    translations: any = {};

    ngOnInit() {
        let hasBeenRead = false;
        const messageId = +this.route.snapshot.paramMap.get('id');

        this.contactsLoading$ = this.contactsStore.Loading$;

        this.store.MessagesPageData$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(ml => {

                if (!ml.messages.length) { return; }

                this.message = ml.messages.filter(m => m.id === messageId)[0];


                if (this.message.readStatus === 0 && !hasBeenRead) {
                    hasBeenRead = true;
                    this.store.setMessageAsRead(messageId);
                }
            });


        this.store.HasBeenFetched$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(fetched => {
                if (!fetched) {
                    this.store.loadMessagesPage();
                }
            });

        // To know wheter is contact
        this.contactsStore.Contacts$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(cl => {
                setTimeout(() => {
                    this.isContact = cl.find(c =>
                        (c.phone && (c.phone === this.message.phone)) &&
                        (c.email && (c.email === this.message.email))
                    ) !== undefined;
                }, 100);
            });

        // To know wheter is contact
        this.contactsStore.HasBeenFetched$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(fetched => {
                if (!fetched) {
                    this.contactsStore.fetchContacts();
                }
            });

        this._handleError();

        this.firebasex.setScreenName('MESSAGES > DETAIL');
    }

    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }

    private _handleError() {
        this.store.Error$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(async (e) => {
                if (e && e.after === 'SET_READ') {
                    const toast = await this.toastController.create({
                        message: this.translations.MESSAGES.ERRORS[e.after.toLowerCase()],
                        duration: 5000,
                        color: 'danger'
                    });
                    toast.present();
                }
            });
    }

    ionViewDidLeave() {
        this.leaved$.next(true);
        this.leaved$.complete();
    }

    async showMessageOpts() {
        const buttons = [{
            text: this.translations.MESSAGES.callContact,
            handler: () => {
                this.sharingSrvc.makeCall(this.message.phone);
            }
        }, {
            text: this.translations.MESSAGES.emailContact,
            handler: () => {
                this.sharingSrvc.sendEmail(this.message.email);
            }
        }, {
            text: this.translations.GENERAL.ACTION.cancel,
            role: 'cancel',
            handler: () => { }
        }];

        if (!this.isContact) {
            buttons.unshift({
                text: this.translations.MESSAGES.addToContactList,
                handler: () => {
                    this.presentModal();
                }
            });
        }


        (await this.actionSheetCtrl.create({
            header: this.translations.GENERAL.contact,
            buttons
        })).present();
    }

    async presentModal() {

        const modal = await this.modalCtrl.create({
            component: ContactUpsertModalComponent,
            componentProps: {
                mode: 'CREATE',
                contact: new ContactModel({
                    id: null,
                    name: this.message.name,
                    lastName: '',
                    type: 'NOT_SPECIFIED',
                    email: this.message.email,
                    phone: this.message.phone
                })
            }
        });

        this.firebasex.setScreenName('MESSAGES > CONTACT > CREATE');

        await modal.present();

        const { data } = await modal.onWillDismiss();
    }
}
