import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MessageDetailPage } from './message-detail/message-detail.page';
import { MessagesPage } from './messages.page';

const routes: Routes = [{
    path: '',
    component: MessagesPage
}, {
    path: 'detail/:id',
    component: MessageDetailPage
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class MessagesPageRoutingModule { }
