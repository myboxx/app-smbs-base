import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MessagesCoreModule } from '@boxx/messages-core';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { MessageDetailPage } from './message-detail/message-detail.page';
import { MessagesPageRoutingModule } from './messages-routing.module';
import { MessagesPage } from './messages.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        MessagesPageRoutingModule,
        ReactiveFormsModule,
        TranslateModule.forChild(),
        PipesModule,
        MessagesCoreModule,
    ],
    declarations: [
        MessagesPage,
        MessageDetailPage
    ],
    providers: []
})
export class MessagesPageModule { }
