import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageModel, MessagesStore, MESSAGE_TYPE } from '@boxx/messages-core';
import { ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FirebasexService } from 'src/services/NativeFirebaseX.service';

@Component({
    selector: 'app-messages',
    templateUrl: './messages.page.html',
    styleUrls: ['./messages.page.scss'],
})
export class MessagesPage implements OnInit, OnDestroy {

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private store: MessagesStore,
        public toastController: ToastController,
        private translate: TranslateService,
        private firebasex: FirebasexService
    ) {
        this.translate.get(['MESSAGES'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe((t: any) => this.translations = t);

        this.route.queryParams
            .pipe(takeUntil(this.destroyed$))
            .subscribe(params => {
                if (this.router.getCurrentNavigation().extras.state) {
                    this.filterType = ((this.router.getCurrentNavigation().extras.state.type || 'all').toUpperCase() as MESSAGE_TYPE);
                }
            });

        this.filterType = ((this.route.snapshot.paramMap.get('type') || 'all').toUpperCase() as MESSAGE_TYPE);

        this.isLoading$ = this.store.Loading$;

        this.filteredMessageList$ = this.store.FilteredMessages$;

        this.store.MessagesPageData$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(d => {
                this.totals.total = d.totals.total;
                this.totals.read = d.totals.read;
                this.totals.new = d.totals.new;
                this.filterBy(this.filterType);
            });

        this.filteredMessageList$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(m => {
                this.loadedMessages = m;
            });

        this.store.HasBeenFetched$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(fetched => {
                if (!fetched) {
                    this.store.loadMessagesPage('ASC');
                }
            });
    }

    filterType: MESSAGE_TYPE = 'ALL';

    filteredMessageList$: Observable<MessageModel[]>;
    destroyed$ = new Subject<boolean>();
    isLoading$: Observable<boolean>;

    totals: { total: number, read: number, new: number } = { total: 0, read: 0, new: 0 };
    loadedMessages: MessageModel[];

    translations: any;

    ngOnInit() {
        this._handleError();
    }

    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }

    ionViewDidEnter() {
        this.firebasex.setScreenName('MESSAGES');
    }

    private _handleError() {
        this.store.Error$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(async (e) => {

                if (e && e.after === 'GET') {
                    const toast = await this.toastController.create({
                        message: this.translations.MESSAGES.ERRORS[e.after.toLowerCase()],
                        duration: 5000,
                        color: 'danger'
                    });
                    toast.present();
                }
            });
    }

    filterBy(criteria: MESSAGE_TYPE = this.filterType) {
        this.store.filterMessages(criteria);
    }

    makeCall(phoneNumber: string) {
        window.location.href = 'tel:' + encodeURIComponent(phoneNumber);
    }

    sendMail(email: string) {
        console.log('SENDING MAIL...');
        window.location.href = 'mailto:' + email;
    }

    toggleReadStatus(message: MessageModel) {
        message.readStatus = message.readStatus === 1 ? 0 : 1;
    }

    doRefresh(event) {
        this.store.loadMessagesPage('ASC');
        setTimeout(() => {
            event.target.complete();
        }, 100);
    }
}
