import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FirebasexService } from 'src/services/NativeFirebaseX.service';
import { LocalStorageNativeService } from 'src/services/NativeLocalStorage.service';

@Component({
    selector: 'app-page-onboarding',
    templateUrl: 'onboarding.page.html',
    styleUrls: ['./onboarding.page.scss'],
})
export class OnboardingPage {
    constructor(
        private router: Router,
        private localStge: LocalStorageNativeService,
        private firebasex: FirebasexService
    ) { this.firebasex.setScreenName('ONBOARDING'); }

    close() {
        this.localStge.setItem('tutorial', 'OK')
            .finally(() => this.router.navigateByUrl('tabs', { skipLocationChange: true, replaceUrl: true }));
    }
}
