import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { EventsCoreModule } from '@boxx/events-core';
import { MessagesCoreModule } from '@boxx/messages-core';
import { PanelCoreModule } from '@boxx/panel-core';
import { SharedModulesModule } from '@boxx/shared-modules';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { PanelPage } from './panel.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([{
            path: '',
            component: PanelPage
        }]),
        PipesModule,
        TranslateModule.forChild(),
        NgxQRCodeModule,
        MessagesCoreModule,
        PanelCoreModule,
        EventsCoreModule,
        SharedModulesModule
    ],
    declarations: [PanelPage],
    providers: []
})
export class PanelPageModule { }
