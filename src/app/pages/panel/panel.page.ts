import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NavigationExtras, NavigationStart, Router } from '@angular/router';
import { ContactModel, ContactStore } from '@boxx/contacts-core';
import { LoginService } from '@boxx/login-core';
import { MessagesPageModel, MessagesStore } from '@boxx/messages-core';
import { PanelPageModel, PanelStore } from '@boxx/panel-core';
import { WebsiteStore } from '@boxx/website-core';
import { Device } from '@ionic-native/device/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { AlertController, MenuController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Chart } from 'chart.js';
import * as moment from 'moment';
import { Observable, Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { FirebasexService } from 'src/services/NativeFirebaseX.service';
import { LocalNotificationsService } from 'src/services/NativeLocalNotification.service';


@Component({
    selector: 'app-panel',
    templateUrl: 'panel.page.html',
    styleUrls: ['panel.page.scss'],
})
export class PanelPage implements OnInit, OnDestroy {
    @ViewChild('contactChart', { static: false }) contactChart;

    constructor(
        private platform: Platform,
        private store: PanelStore,
        private messagesStore: MessagesStore,
        private websiteStore: WebsiteStore,
        private contactStore: ContactStore,
        private router: Router,
        private authSrvc: LoginService,
        private menuCtrl: MenuController,
        public toastController: ToastController,
        private translate: TranslateService,
        private device: Device,
        private localNotifications: LocalNotifications,
        private localNotifSrvc: LocalNotificationsService,
        private firebasex: FirebasexService,
        public popoverCtrl: PopoverController,
        private alertCtrl: AlertController
    ) {
        this.translate.get(['GENERAL', 'PANEL'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe((t: any) => this.translations = t);

        this.setupPushNotifications();

        this.listenLocalNotifications();

        router.events.pipe(filter(event => event instanceof NavigationStart))
            .subscribe((event: NavigationStart) => {
                if (this.toast) {
                    this.toast.dismiss();
                }
            });
    }

    isLoading$: Observable<boolean>;
    errors$: Observable<any>;
    panel$: Observable<PanelPageModel>;
    contacts$: Observable<ContactModel[]>;

    filteredContacts$: Observable<ContactModel[]>;
    messages$: Observable<MessagesPageModel>;
    destroyed$ = new Subject<boolean>();

    toast: any;

    lastContacts: Array<ContactModel> = [];

    contactsData = {
        stats: {
            total: 0,
            client: 0,
            prospect: 0,
            not_specified: 0
        }
    };

    messagesData = {
        total: 0, read: 0, unread: 0
    };

    panelData = { userName: '', upToDate: moment().toISOString(true) };

    chart: Chart;

    translations: any;

    isLoadingWebsiteData$: Observable<boolean>;

    websiteUrl: string = null;

    ngOnInit() {
        this.isLoading$ = this.store.Loading$;
        this.panel$ = this.store.Panel$;
        this.messages$ = this.messagesStore.MessagesPageData$;

        this.authSrvc.getUserName().then(name => this.panelData.userName = name);

        const chartData = {
            responsive: false,
            datasets: [{
                // set values in order for: [client, prospect, not_specified]:
                data: [],
                backgroundColor: ['#3A8340', '#0032E6', '#6F7779']
            }],
        };

        setTimeout(() => {
            this.chart = new Chart(this.contactChart.nativeElement, {
                type: 'doughnut',
                data: chartData,
                options: {
                    cutoutPercentage: 85,
                }
            });

            this.loadPanelData();
        }, 1);

        this._handleError();
    }

    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }

    private _handleError() {
        this.store.Error$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(async (e) => {

                if (e && e.after === 'GET') {
                    this._showToast({
                        message: this.translations.PANEL.ERRORS[e.after.toLowerCase()],
                        // duration: 5000,
                        position: 'top',
                        color: 'danger',
                        buttons: [
                            {
                                side: 'start',
                                icon: 'close-outline',
                                handler: () => {
                                    if (this.toast) {
                                        this.toast.dismiss();
                                    }
                                }
                            },
                            {
                                side: 'end',
                                icon: 'reload-outline',
                                // text: 'Favorite',
                                handler: () => {
                                    this.store.loadPanel();
                                }
                            }
                        ]
                    });
                }
            });
    }

    ionViewDidEnter() {
        this.menuCtrl.swipeGesture(true);

        this.firebasex.setScreenName('PANEL');
    }

    ionViewDidLeave() {
        this.menuCtrl.swipeGesture(false);

        if (this.toast) {
            this.toast.dismiss();
        }
    }

    loadPanelData() {

        this.contactStore.Contacts$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(cl => {
                this.lastContacts = cl.slice(0, 3);

                let clients = 0;
                let prospect = 0;
                let notSpecified = 0;

                cl.forEach((c) => {
                    if (c.type === 'CLIENT') {
                        clients++;
                    }
                    else if (c.type === 'PROSPECT') {
                        prospect++;
                    }
                    else {
                        notSpecified++;
                    }
                });

                const values = this.computeContactsPercentages({
                    total: cl.length,
                    clients,
                    prospect,
                    notSpecified
                });

                this.initChart(values);
            });

        this.panel$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(panel => {
                this.lastContacts = panel.contactsData.lastContacts.slice(0, 3);

                const total = panel.contactsData.stats.total;
                const clients = panel.contactsData.stats.client;
                const prospect = panel.contactsData.stats.prospect;
                const notSpecified = panel.contactsData.stats.notSpecified;

                const values = this.computeContactsPercentages({
                    total,
                    clients,
                    prospect,
                    notSpecified
                });

                // set values in order for: [client, prospect, not_specified]
                this.initChart(values);
            });
        this.store.loadPanel();

        this.isLoadingWebsiteData$ = this.websiteStore.Loading$;

        this.websiteStore.Website$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(async (websitePage) => {
                const { websiteList } = websitePage;
                if (websiteList.length) {
                    this.websiteUrl = websiteList[0].siteUrl || null;
                }
            });
        this.websiteStore.LoadWebsite();

        this.messagesStore.loadMessagesPage();
    }

    computeContactsPercentages(stats: { total: number, clients: number, prospect: number, notSpecified: number }) {
        const total = this.contactsData.stats.total = stats.total;

        const clients = stats.clients;
        const prospect = stats.prospect;
        const notSpecified = stats.notSpecified;

        this.contactsData.stats.client = +(clients / total * 100).toFixed(0) || 0;
        this.contactsData.stats.prospect = +(prospect / total * 100).toFixed(0) || 0;
        this.contactsData.stats.not_specified = +(notSpecified / total * 100).toFixed(0) || 0;

        return [
            clients,
            prospect,
            notSpecified
        ];
    }

    initChart(values: Array<number>) {
        this.chart.data.datasets.forEach((dataset) => {
            // set values in order for: [client, prospect, not_specified]
            dataset.data = values;
        });

        this.chart.update();
    }

    doRefresh(event: any) {
        if (this.toast) {
            this.toast.dismiss();
        }

        this.store.loadPanel();
        this.websiteStore.LoadWebsite();
        this.messagesStore.loadMessagesPage();

        setTimeout(() => {
            event.target.complete();
        }, 100);
    }

    goToMessages(type: string) {
        const navigationExtras: NavigationExtras = { state: { type } };
        this.router.navigateByUrl('/tabs/messages', navigationExtras);
    }

    goToContacts(){
        this.router.navigateByUrl('/tabs/contacts');
    }

    goWebsiteStats() {
        const navigationExtras: NavigationExtras = { state: { type: 'STATS' } };
        this.router.navigateByUrl('/tabs/website', navigationExtras);
    }

    goToEvent(eventId: number) {
        const navigationExtras: NavigationExtras = { state: { eventId } };
        this.router.navigateByUrl('/tabs/events', navigationExtras);
    }

    private setupPushNotifications() {
        if (this.platform.is('android')) {
            this.firebasex.getToken().then(token => {
                if (token) {
                    this.authSrvc.saveToken(this.device.uuid, token).subscribe();
                }
                else {
                    console.error('No se pudo obtener el token (Android)');

                    // todo: log event here!
                }
            });
        }
        else {
            this.firebasex.getAPNSToken().then(token => {
                if (token) {
                    this.authSrvc.saveToken(this.device.uuid, token).subscribe();
                }
                else {
                    console.error('No se pudo obtener el token (ios)');

                    // todo: log event here!
                }
            });
        }

        this.firebasex.onMessageReceived().subscribe(async (msg) => {
            console.log('--- onMessageReceived:', msg);

            if (msg.view) {
                switch (msg.view) {
                    case 'leads':
                        this.router.navigateByUrl('/tabs/messages');
                        break;
                    case 'events':
                        this.router.navigateByUrl('/tabs/events');
                        break;
                    case 'contacts':
                        this.router.navigateByUrl('/tabs/contacts');
                        break;
                    case 'app_update':
                        (await this.alertCtrl.create({
                            header: this.translations.GENERAL.appUpdate,
                            message: this.translations.GENERAL.appUpdateMsg,
                            buttons: [{
                                text: this.translations.GENERAL.ACTION.ok
                            }]
                        })).present();
                        break;
                }
            }
        }, error => {
            console.error('onMessageReceived:', error);
        });
    }

    private listenLocalNotifications() {
        this.localNotifSrvc.init().then(ok => {
            if (ok) {
                this.localNotifications.on('click').subscribe((notification) => {
                    this.goToEvent(notification.id);
                });
            }
        });
    }

    private async _showToast(options: { header?, message, color?: string, buttons?, duration?: number, position?}) {
        this.toast = await this.toastController.create(options);
        this.toast.present();
    }
}
