import { Component, OnDestroy, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { APP_CONSTANTS } from 'src/constants/constants';
import { FirebasexService } from 'src/services/NativeFirebaseX.service';
import { SharingService } from 'src/services/NativeSharing.service';

@Component({
    selector: 'app-faqs',
    templateUrl: './faqs.page.html',
    styleUrls: ['./faqs.page.scss'],
})
export class FaqsPage implements OnInit, OnDestroy {

    constructor(
        private menuCtrl: MenuController,
        private firebasex: FirebasexService,
        private sharingSrvc: SharingService
    ) { }

    faqs: Array<any> = [
        { title: '¿Cómo contrato la aplicación móvil?', description: 'La aplicación móvil <b>Espacio Emprendedor</b> es exclusiva para clientes que hayan contratado el servicio de <b>Mi Negocio Digital</b> para la creación de su "Sitio Web"' },
        { title: '¿Cómo descargo la aplicación móvil Mi Negocio Digital de Espacio Emprendedor?', description: 'La aplicación móvil <b>Mi  Negocio  Digital</b>  de  <b>Espacio  Emprendedor</b> la podrás descargar de las tiendas de aplicaciones de <i><b>App Store</b></i> y <i><b>Google Play</b><i>.' },
        { title: '¿Cómo ingreso a la aplicación móvil Mi Negocio Digital de Espacio Emprendedor?', description: 'Una vez que descargaste la aplicación, ingresa con las credenciales de acceso que se enviaron al correo electrónico que registraste. Si lo deseas, puedes modificar tu contraseña ingresando a la opción "Olvidé mi contraseña" y recibirás un correo electrónico con las instrucciones para modificarla.' },
        { title: 'Si olvidé mi usuario o contraseña, ¿cómo puedo recuperar mis datos de acceso?', description: 'En la pantalla de inicio, al momento de querer ingresar a la aplicación, vas a encontrar la opción "Olvidé mi contraseña"; sólo debes escribir tu correo electrónico y recibirás un correo de respuesta con las instrucciones para restaurarla.' },

        { title: '¿Existen requerimientos de sistema para instalar la aplicación?', description: 'iOS 10.0 en adelante. Es importante que consideres que Apple pronto dejará de dar soporte a las pantallas de 3.5 pulgadas. Android 6, recomendable 7.' },

        { title: '¿Cómo actualizo mi App?', description: 'Debes ingresar a la tienda de aplicaciones de tu dispositivo y verificar si hay alguna actualización pendiente de <b>Mi Negocio Digital de Espacio Emprendedor.</b>' },

        { title: '¿Qué acciones puedo realizar con mi aplicación móvil?', description: 'La aplicación <b>Mi Negocio Digital de Espacio Emprendedor</b> te permite visualizar las estadísticas principales de tu Sitio Web; consultar y dar seguimiento a los mensajes recibidos desde tu sitio web; agregar, clasificar y consultar tus contactos de negocio; agendar eventos; así como acceder a contenido especializado con tips para que logres incrementar las ventas de tu negocio.' },

        /*{
            title: '¿Dónde puedo visualizar las visitas generadas a mi Sitio Web?',
            description: 'En la sección WEBSITE – apartado – ESTADÍSTICAS, puedes visualizar el número de visitas que tiene tu sitio web.'
        },*/
        { title: '¿Cómo puedo administrar mi Sitio Web Espacio Emprendedor?', description: 'En la sección MI NEGOCIO puedes actualizar la información de tu sitio web, así como realizar algunos cambios de texto en la descripción general de tu negocio, horarios, productos y/o servicios.' },
        { title: 'Cómo puedo visualizar mi Sitio Web Espacio Emprendedor?', description: 'En la sección MI  NEGOCIO – apartado – ADMINISTRAR, opción “Ver sitio web”, puedes visualizarlo.' },
        { title: '¿Dónde puedo consultar los mensajes que los visitantes han dejado en mi Sitio Web Espacio Emprendedor?', description: 'En la aplicación – sección –MENSAJES, puedes ver los mensajes Vistos, Nuevos y Totales.' },
        { title: '¿Cómo puedo agendar un evento dentro de aplicación móvil?', description: 'En la aplicación – sección – EVENTOS, puedes agregar eventos y programar citas con tus clientes.' },
        /*{
            title: '¿Cómo puedo ingresar al blog dentro de Espacio Emprendedor?',
            description: 'En la aplicación – sección – BLOG, '+
            'puedes consultar artículos que te ayudarán a gestionar tu negocio de la mejor manera.'
        },*/
        { title: '¿Cómo agregar un contacto de negocio desde la aplicación Espacio Emprendedor?', description: 'En la aplicación – sección – CONTACTOS, puedes agregar, consultar y clasificar a tus contactos en Prospectos, Oportunidades y Clientes.' },
    ];

    shownGroup: any;

    productPageUrl = APP_CONSTANTS.PRODUCT_PAGE_URL;
    privacyPolicy = APP_CONSTANTS.PRIVACY_POLICY_URL;

    ngOnInit() {
        this.menuCtrl.swipeGesture(false);
        this.firebasex.setScreenName('FAQS');
    }

    ngOnDestroy() {
        this.menuCtrl.swipeGesture(true);
    }

    toggleGroup(group) {
        if (this.isGroupShown(group)) {
            this.shownGroup = null;
        } else {
            this.shownGroup = group;
        }
    }

    isGroupShown(group) {
        return this.shownGroup === group;
    }

    openLink(url: string) {
        if (url === 'PRODUCT_PAGE_URL'){
            this.sharingSrvc.openURL(APP_CONSTANTS.PRODUCT_PAGE_URL);
        }
        else if (url === 'PRIVACY_POLICY_URL'){
            this.sharingSrvc.openURL(APP_CONSTANTS.PRIVACY_POLICY_URL);
        }
    }
}
