import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginStore } from '@boxx/login-core';
import { ProfileModel, ProfileStore } from '@boxx/profile-core';
import { AlertController, MenuController, ModalController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ProfileUpdateModalComponent } from 'src/app/pages/profile/profile-update-modal/profile-update.modal';
import { FirebasexService } from 'src/services/NativeFirebaseX.service';
import { LocalNotificationsService } from 'src/services/NativeLocalNotification.service';


@Component({
    selector: 'app-profile-detail',
    templateUrl: './profile-detail.page.html',
    styleUrls: ['./profile-detail.page.scss'],
})
export class ProfileDetailPage implements OnInit, OnDestroy {

    constructor(
        private formBuilder: FormBuilder,
        private store: ProfileStore,
        private modalCtrl: ModalController,
        private alertCtrl: AlertController,
        private translate: TranslateService,
        public toastController: ToastController,
        private router: Router,
        private menuCtrl: MenuController,
        private loginStore: LoginStore,
        private localNotifSrvc: LocalNotificationsService,
        private firebasex: FirebasexService
    ) {
        this.translate.get(['GENERAL', 'LOGIN', 'PROFILE'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe((t: any) => this.translations = t);
    }

    profileForm: FormGroup;
    profile: ProfileModel;

    profile$: Observable<ProfileModel>;
    isLoading$: Observable<boolean>;
    destroyed$ = new Subject<boolean>();

    translations: any = {};

    ngOnInit() {
        this.menuCtrl.swipeGesture(false);

        this.profileForm = this.formBuilder.group({
            userFullName: '',
            phone: '',
            email: '',
            userId: ''
        });

        this.profileForm.disable();

        this.isLoading$ = this.store.Loading$;

        this.store.Profile$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(p => {
                if (p.userId) {
                    this.profile = p;

                    this.profileForm.patchValue({
                        userFullName: p.name,
                        phone: p.phoneNumber,
                        email: p.userEmail,
                        userId: p.userId
                    });
                }
            });

        this.store.LoadProfile();

        this.loginStore.User$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(u => {
                if (!u.id) {
                    this.localNotifSrvc.stop();
                    this.router.navigateByUrl('login', { skipLocationChange: true, replaceUrl: true });
                }
            });

        this._handleSuccess();
        this._handleError();

        this.firebasex.setScreenName('PROFILE');
    }

    ngOnDestroy() {
        this.menuCtrl.swipeGesture(true);
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }

    private _handleSuccess() {
        this.store.Success$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(async (success) => {

                if (success) {
                    const toast = await this.toastController.create({
                        message: this.translations.PROFILE.SUCCESS[success.after.toLowerCase()],
                        duration: 3000,
                        color: 'success',
                    });
                    toast.present();
                }
            });
    }

    private _handleError() {
        this.store.Error$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(async (e) => {

                if (e) {
                    const toast = await this.toastController.create({
                        message: this.translations.PROFILE.ERRORS[e.after.toLowerCase()],
                        duration: 5000,
                        color: 'danger',
                    });
                    toast.present();
                }
            });
    }

    async presentModal() {
        const modal = await this.modalCtrl.create({
            component: ProfileUpdateModalComponent,
            componentProps: {
                profile: this.profile
            }
        });

        this.firebasex.setScreenName('PROFILE > UPDATE');

        await modal.present();

        const { data } = await modal.onWillDismiss();

        if (data) {
            this.store.UpdateProfile(data);
        }
    }

    async doLogout() {
        (await this.alertCtrl.create({
            header: this.translations.GENERAL.ACTION.confirm,
            message: this.translations.LOGIN.logoutConfirm,
            buttons: [{
                text: this.translations.GENERAL.ACTION.cancel,
                role: 'cancel',
                cssClass: 'primary',
            }, {
                text: this.translations.GENERAL.ACTION.ok,
                handler: () => {
                    this.loginStore.logout();
                }
            }]
        })).present();
    }
}
