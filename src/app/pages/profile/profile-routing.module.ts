import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FaqsPage } from './faqs/faqs.page';
import { ProfileDetailPage } from './profile-detail/profile-detail.page';
import { SupportPage } from './support/support.page';
import { TAndCPage } from './t-and-c/t-and-c.page';

const routes: Routes = [{
    path: 'detail',
    component: ProfileDetailPage,
},
{
    path: 'faqs',
    component: FaqsPage
},
{
    path: 'support',
    component: SupportPage
},
{
    path: 'terms',
    component: TAndCPage
},
{
    path: '',
    component: ProfileDetailPage
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ProfilePageRoutingModule { }
