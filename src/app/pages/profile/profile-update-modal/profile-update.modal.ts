import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProfileModel } from '@boxx/profile-core';
import { ModalController } from '@ionic/angular';
import { APP_CONSTANTS } from 'src/constants/constants';

@Component({
    selector: 'app-profile-update-modal',
    templateUrl: './profile-update.modal.html',
    styleUrls: ['./profile-update.modal.scss'],
})
export class ProfileUpdateModalComponent implements OnInit {
    @Input() profile: ProfileModel;

    profileForm: FormGroup;

    validationMessages = {
        userFullName: [
            { type: 'required', message: '•El nombre es requerido' },
            { type: 'pattern', message: '•Sólo letras' },
        ],
        phone: [
            { type: 'required', message: '•El teléfono es requerido' },
            { type: 'pattern', message: '•Sólo debe contener números' },
            { type: 'minlength', message: '•Deben ser exactamente 10 dígitos' },
            { type: 'maxlength', message: '•Deben ser exactamente 10 dígitos' },
        ],
        email: [
            { type: 'required', message: '•El email es requerido' },
            { type: 'pattern', message: '•Formato de email incorrecto' }
        ],
    };

    constructor(private formBuilder: FormBuilder,
                private modalCtrl: ModalController) { }

    ngOnInit() {
        this.profileForm = this.formBuilder.group({
            userFullName: [this.profile.name, [Validators.required, /* Validators.pattern('[a-zA-ZÀ-ȕ ]+')*/]],
            phone: [this.profile.phoneNumber, [Validators.minLength(10), Validators.maxLength(10), Validators.required, Validators.pattern('[0-9]+')]],
            email: [this.profile.userEmail, [Validators.required, Validators.pattern(APP_CONSTANTS.EMAILPATTERN)]],
            userId: this.profile.userId
        });

        this.profileForm.disable();

        this.profileForm.get('userFullName').enable();
        this.profileForm.get('email').enable();
    }

    close() {
        this.modalCtrl.dismiss(/*{someData: ...} */);
    }

    update() {
        this.modalCtrl.dismiss({
            name: this.profileForm.get('userFullName').value,
            email: this.profileForm.get('email').value,
        });
    }
}
