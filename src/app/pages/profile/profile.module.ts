import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Device } from '@ionic-native/device/ngx';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { ProfileUpdateModalComponent } from 'src/app/pages/profile/profile-update-modal/profile-update.modal';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { FaqsPage } from './faqs/faqs.page';
import { ProfileDetailPage } from './profile-detail/profile-detail.page';
import { ProfilePageRoutingModule } from './profile-routing.module';
import { SupportPage } from './support/support.page';
import { TAndCPage } from './t-and-c/t-and-c.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ProfilePageRoutingModule,
        ReactiveFormsModule,
        TranslateModule.forChild(),
        PipesModule
    ],
    declarations: [
        ProfileDetailPage,
        ProfileUpdateModalComponent,
        FaqsPage,
        SupportPage,
        TAndCPage
    ],
    providers: [
        Device,
        AppVersion,
    ],
    entryComponents: [],
})
export class ProfilePageModule { }
