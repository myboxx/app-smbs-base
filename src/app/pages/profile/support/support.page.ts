import { Component, OnDestroy, OnInit } from '@angular/core';
import { LoginService } from '@boxx/login-core';
import { WebsiteStore } from '@boxx/website-core';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Device } from '@ionic-native/device/ngx';
import { LoadingController, MenuController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { APP_CONSTANTS } from 'src/constants/constants';
import { FirebasexService } from 'src/services/NativeFirebaseX.service';
import { SharingService } from 'src/services/NativeSharing.service';

@Component({
    selector: 'app-support',
    templateUrl: './support.page.html',
    styleUrls: ['./support.page.scss'],
})
export class SupportPage implements OnInit, OnDestroy {

    constructor(
        private menuCtrl: MenuController,
        private sharingSrvc: SharingService,
        private device: Device,
        private appVersion: AppVersion,
        private authSrvc: LoginService,
        private websiteStore: WebsiteStore,
        private loadingCtrl: LoadingController,
        private toastController: ToastController,
        private translate: TranslateService,
        private firebasex: FirebasexService
    ) {
        this.translate.get(['ERRORS'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe((t: any) => {
                this.translations = t;
            });
    }

    loader: any;
    translations: any;
    destroyed$ = new Subject<boolean>();

    ngOnInit() {
        this.menuCtrl.swipeGesture(false);

        this.firebasex.setScreenName('SUPPORT');
    }

    ngOnDestroy() {
        this.menuCtrl.swipeGesture(true);
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }

    async sendSupportEmail() {
        const session = await this.authSrvc.getSession();

        this.loader = await this.loadingCtrl.create();
        this.loader.present();

        this.websiteStore.Website$
            .pipe(
                takeUntil(this.destroyed$),
                map(website => website.websiteList[0])
            ).subscribe(async (websitePage) => {

                if (!websitePage.siteId) { return; }

                const deviceInfo = {
                    manufacturer: this.device.manufacturer,
                    model: this.device.model,
                    osName: this.device.platform,
                    version: this.device.version
                };

                const appInfo = {
                    version: await this.appVersion.getVersionNumber(),
                    buildNumber: await this.appVersion.getVersionCode()
                };

                function _newLine(text: string) {
                    const BLACK_LIST = ['ELE-L29', 'LG-H500'];
                    const newLine = BLACK_LIST.indexOf(deviceInfo.model.toUpperCase()) !== -1 ? '</br>' : '\n';
                    return encodeURIComponent(`${newLine}${text}`);
                }

                const subject = `?subject=${APP_CONSTANTS.SUPPORT_SUBJECT}&body=`;
                const container = `${APP_CONSTANTS.SUPPORT_EMAIL_ADDRESS}`;
                const separator = _newLine('------------ SUPPORT REQUEST ------------');
                const userEmail = _newLine('User email: ' + session.email);
                const siteUrl = _newLine('Site url: ' + websitePage.siteUrl);
                const siteId = _newLine('Site id: ' + websitePage.siteId);
                const appVersion = _newLine(`App Version: ${appInfo.version} build: ${appInfo.buildNumber}`);
                const manufacturer = _newLine(`Manufacturer: ${deviceInfo.manufacturer}`);
                const model = _newLine(`Model: ${deviceInfo.model}`);
                const osName = _newLine(`OS Name:  ${deviceInfo.osName}`);
                const osVersion = `OS Version: ${deviceInfo.version}`;
                const body = separator + userEmail + siteUrl + siteId + appVersion + manufacturer + model + osName + osVersion;
                const url = container + subject + body;

                this.loadingCtrl.dismiss().then(() => {
                    this.loader = null;
                    this.sharingSrvc.sendEmail(url);
                });
            });

        this.websiteStore.LoadWebsite();
    }

    callForSupport() {
        this.sharingSrvc.makeCall(APP_CONSTANTS.SUPPORT_PHONE);
    }

    async presentToast() {
        const toast = await this.toastController.create({
            message: this.translations.ERRORS.unknown,
            duration: 2000
        });
        toast.present();
    }
}
