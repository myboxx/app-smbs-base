import { Component, OnDestroy, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { FirebasexService } from 'src/services/NativeFirebaseX.service';

@Component({
    selector: 'app-t-and-c',
    templateUrl: './t-and-c.page.html',
    styleUrls: ['./t-and-c.page.scss'],
})
export class TAndCPage implements OnInit, OnDestroy {

    constructor(
        private menuCtrl: MenuController,
        private firebasex: FirebasexService
    ) { }

    ngOnInit() {
        this.menuCtrl.swipeGesture(false);

        this.firebasex.setScreenName('T&C');
    }

    ngOnDestroy() {
        this.menuCtrl.swipeGesture(true);
    }

}
