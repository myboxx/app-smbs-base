import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [{
    path: '',
    component: TabsPage,
    children: [{
        path: 'panel',
        loadChildren: () => import('../panel/panel.module').then(m => m.PanelPageModule)
    },
    {
        path: 'messages',
        loadChildren: () => import('../messages/messages.module').then(m => m.MessagesPageModule)
    },
    {
        path: 'contacts',
        children: [{
            path: '',
            loadChildren: () =>
                import('../contacts/contacts.module').then(m => m.ContactsPageModule)
        },
        {
            path: 'contact-detail/:id',
            // loadChildren: () => import('../contact-detail/contact-detail.module')
            // .then(m => m.ContactDetailPageModule)
        }
        ]
        // loadChildren: () => import('../contacts/contacts.module')
        // .then( m => m.ContactsPageModule)
    },
    {
        path: 'website',
        loadChildren: () => import('../website/website.module').then(m => m.WebsitePageModule)
    },
    /*{
      path: 'profile',
      loadChildren: () => import('../profile/profile.module')
      //.then( m => m.ProfilePageModule)
    },*/
    {
        path: 'events',
        loadChildren: () => import('../events/events.module').then(m => m.EventsPageModule)
    },
    {
        path: '',
        redirectTo: 'panel',
        pathMatch: 'full'
    }]
},
{
    path: '',
    redirectTo: 'panel',
    pathMatch: 'full'
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TabsPageRoutingModule { }
