import { Component, OnInit } from '@angular/core';
import { MessagesStore } from '@boxx/messages-core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-tabs',
    templateUrl: 'tabs.page.html',
    styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit {

    constructor(private messagesStore: MessagesStore) { }

    destroyed$ = new Subject<boolean>();

    numUnreadMessages = 0;

    ngOnInit() {
        this.messagesStore.MessagesPageData$
            .pipe(takeUntil(this.destroyed$))
            .subscribe((messagesData) => {
                this.numUnreadMessages = messagesData.totals.new;
            });
    }
}
