import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { CameraOptions } from '@ionic-native/camera/ngx';
import { CropOptions } from '@ionic-native/crop/ngx';
import { ImagePickerOptions } from '@ionic-native/image-picker/ngx';
import { AlertController, IonSlides, ModalController, ToastController } from '@ionic/angular';
import { CameraNativeService } from 'src/services/NativeCamera.service';
import { FileNativeService } from 'src/services/NativeFile.service';

@Component({
    selector: 'app-image-picker-viewer',
    templateUrl: './image-picker-viewer.component.html',
    styleUrls: ['./image-picker-viewer.component.scss']
})
export class ImagePickerViewerComponent implements OnInit, OnDestroy {
    @ViewChild(IonSlides, { static: false }) slider: IonSlides;

    @Input() source: PICKER_SOURCE_TYPE;
    @Input() showUploadBtn: boolean;
    @Input() images: string[] = []; // images to show (viewer)
    @Input() imagesErrors: Array<string[]> = []; // image descriptions to show (viewer)
    @Input() maxImages: number;
    @Input() forGallery: boolean;

    // @Input() activeIndex: number;
    @Input() maxDimensions: { width: number, height: number };
    @Input() maxSizeInMB: number;

    // icon (name/source) to show as ok button
    @Input() okIconName: string;
    @Input() okIconSrc: string;

    constructor(
        private modalCtrl: ModalController,
        private toastCtrl: ToastController,
        private cameraService: CameraNativeService,
        private sanitizer: DomSanitizer,
        private fileService: FileNativeService,
        private alertCtrl: AlertController
    ) { }

    public imageList: IMGVALIDATED[] = [];

    sliderOpts = {
        zoom: false,
        centeredSlides: true,
        slidesPerView: 1.5,
        spaceBetween: 20
    };

    siteId: number;

    ngOnInit(): void {
    }

    ngOnDestroy() {
    }

    ionViewDidEnter() {
        if (!this.okIconName && !this.okIconSrc) {
            this.okIconName = 'cloud-upload-outline';
        }

        this.imageList = this.images.map((uri): IMGVALIDATED => {
            // we only need to know that is valid and tue uri
            return {
                valid: true,
                dimensionResult: { valid: true, width: 0, height: 0 },
                fileSizeResult: { valid: true, sizeInMB: 0 },
                uri,
            };
        });

        if (this.slider) {
            this.slider.update();
        }

        if (this.showUploadBtn) {
            this.pickImages();
        }
    }


    close() {
        this.modalCtrl.dismiss();
    }

    pickImages(changeCurrentImage = false) {
        this.source === 'camera' ? this.openCamera() : this.openGallery(changeCurrentImage);
    }

    openCamera() {
        const customOptions: CameraOptions = {};

        if (this.maxDimensions) {
            customOptions.targetWidth = this.maxDimensions.width;
            customOptions.targetHeight = this.maxDimensions.height;
        }

        // JPEG : 0    Return JPEG encoded image
        // PNG : 1     Return PNG encoded image
        customOptions.encodingType = 0;

        customOptions.quality = 75;

        this.cameraService.openCameraMedia('camera', customOptions).then(data => {
            this.imageList = [{
                valid: true,
                dimensionResult: { valid: true, width: 0, height: 0 },
                fileSizeResult: { valid: true, sizeInMB: 0 },
                uri: 'data:image/png;base64,' + data,
            }];
        }, error => {
            if (!['Selection cancelled.', 'No Image Selected'].includes(error)) {
                this.toastCtrl.create({
                    message: 'No se pudo cargar la imagen',
                    duration: 5000,
                    position: 'bottom',
                    color: 'danger'
                }).then(t => t.present());
                console.error('**** pickImage error:', error);
            }
            else {
                if (!this.imageList.length) {
                    this.modalCtrl.dismiss();
                }
            }
        }).catch(e => console.error('---- openCameraMedia', e));
    }

    openGallery(changeCurrentImage = false) {
        const customOptions: ImagePickerOptions = {};

        if (this.maxDimensions) {
            customOptions.width = this.maxDimensions.width;
            customOptions.height = this.maxDimensions.height;
        }

        customOptions.maximumImagesCount = changeCurrentImage ? 1 : this.maxImages - this.imageList.length;
        customOptions.quality = 75;

        this.cameraService.openImagePicker(customOptions).then(async (selImages: any[]) => {
            if (!selImages.length && !this.imageList.length) {
                return this.modalCtrl.dismiss();
            }

            const normalizedImgList: IMGVALIDATED[] = selImages.map((src: string) => {
                return {
                    valid: false,
                    dimensionResult: { valid: false, width: 0, height: 0 },
                    fileSizeResult: { valid: false, sizeInMB: 0 },
                    errors: [], // when an error ocurred while validating
                    uri: src.startsWith('file') ? src : 'data:image/png;base64, ' + src
                };
            });

            const dimensionValidationPromises: Promise<
                { status: 'fulfilled', value: { valid: boolean, width: number, height: number } } |
                { status: 'rejected', reason: any }>[] = [];
            const fileSizeValidationPromises: Promise<{ status: 'fulfilled', value: { valid: boolean, sizeInMB: number } } | { status: 'rejected', reason: any }>[] = [];

            normalizedImgList.forEach(async normalizedImg => {

                dimensionValidationPromises.push(
                    new Promise((resolve) => {
                        if (this.maxDimensions) {
                            this.validateImageDimensions(normalizedImg.uri)
                                .then(response => resolve({ status: 'fulfilled', value: response }))
                                .catch(error => resolve({ status: 'rejected', reason: error }));
                        }
                        else {
                            resolve({ status: 'fulfilled', value: { valid: true, width: 0, height: 0 } });
                        }
                    })
                );

                fileSizeValidationPromises.push(
                    new Promise((resolve) => {
                        if (this.maxDimensions) {
                            this.validateImageFileSize(normalizedImg.uri)
                                .then(response => resolve({ status: 'fulfilled', value: response }))
                                .catch(error => resolve({ status: 'rejected', reason: error }));
                        }
                        else {
                            resolve({ status: 'fulfilled', value: { valid: true, sizeInMB: 0 } });
                        }
                    })
                );
            });

            const P1 = Promise.all(dimensionValidationPromises).then(promises => {
                promises.forEach((res, index) => {
                    if (res.status === 'fulfilled') {
                        normalizedImgList[index].valid = res.value.valid && normalizedImgList[index].fileSizeResult.valid;
                        normalizedImgList[index].dimensionResult = res.value;
                        if (!normalizedImgList[index].dimensionResult.valid) {
                            normalizedImgList[index].dimensionResult.errors =
                                `<b>
                                ${normalizedImgList[index].dimensionResult.width}x${normalizedImgList[index].dimensionResult.height}
                                </b>px superan las dimensiones máximas de <b>
                                ${this.maxDimensions.width}x${this.maxDimensions.height}</b>
                                px<br>`;
                        }
                    }
                    else {
                        normalizedImgList[index].dimensionResult.errors = res.reason;
                    }
                });
            });

            const P2 = Promise.all(fileSizeValidationPromises).then(promises => {
                promises.forEach((res, index) => {
                    if (res.status === 'fulfilled') {
                        normalizedImgList[index].valid = res.value.valid && normalizedImgList[index].dimensionResult.valid;
                        normalizedImgList[index].fileSizeResult = res.value;
                        if (!normalizedImgList[index].fileSizeResult.valid) {
                            normalizedImgList[index].fileSizeResult.errors =
                                `${normalizedImgList[index].fileSizeResult.sizeInMB}
                                Mb supera el máximo de <b>${this.maxSizeInMB}</b>
                                Mb<br>`;
                        }
                    }
                    else {
                        normalizedImgList[index].fileSizeResult.errors = res.reason;
                    }
                });
            });

            Promise.all([P1, P2]).then(_ => {
                if (normalizedImgList.length === 1 && !normalizedImgList[0].valid) {
                    // selected one image but maybe needs to be cropped...
                    if (normalizedImgList[0].dimensionResult.errors && !normalizedImgList[0].fileSizeResult.errors) {
                        const cropOtps: CropOptions = {
                            quality: 100,
                            targetWidth: this.maxDimensions.width,
                            targetHeight: this.maxDimensions.height
                        };

                        this.cameraService.cropImage(normalizedImgList[0].uri, cropOtps)
                            .then(async uri => {
                                normalizedImgList[0].dimensionResult = await this.validateImageDimensions(uri);
                                normalizedImgList[0].uri = uri;
                                this.imageList = normalizedImgList;
                            })
                            .catch(error => {
                                if (error.code !== 'userCancelled') {
                                    this.presentAlert(`No será posible subir la imagen ya que:<br>${error.message || error}`);
                                }
                            });
                    }
                    else {
                        const msg = `
                            ${normalizedImgList[0].dimensionResult.errors}
                            ${normalizedImgList[0].fileSizeResult.errors}
                        `;

                        this.presentAlert(`No será posible subir la imagen ya que:<br>${msg}`);
                    }
                }
                else {
                    this.imageList = normalizedImgList;
                }
            });
        }, error => {
            if (!['Selection cancelled.', 'No Image Selected'].includes(error)) {
                this.toastCtrl.create({
                    message: 'No se pudo cargar la imagen',
                    duration: 5000,
                    position: 'bottom',
                    color: 'danger'
                }).then(t => t.present());
                console.error('**** pickImage error:', error);
            }
        });
    }

    ok() {
        this.modalCtrl.dismiss({ images: this.imageList.map(img => img.uri) });
    }

    getSanitizedUrl(url: string) {
        if (!url) { return 'assets/icon/images.svg'; }

        return url.startsWith('file') ?
            (window as any).Ionic.WebView.convertFileSrc(url) :
            this.sanitizer.bypassSecurityTrustUrl(url);
    }

    validateImageDimensions(imageUri: string): Promise<{ valid: boolean, width: number, height: number }> {
        return this.fileService.getImgDimensions(
            this.getSanitizedUrl(imageUri)
        ).then(resp => {
            console.log(`--- IMG HAS DIMENSIONS: ${resp.width} x ${resp.height}`);
            return {
                valid: (resp.width <= this.maxDimensions.width) && (resp.height <= this.maxDimensions.height),
                width: resp.width,
                height: resp.height
            };
        }, error => error);
    }

    validateImageFileSize(imageUri: string): Promise<{ valid: boolean, sizeInMB: number }> {
        return this.fileService.getImgFileSize(imageUri)
            .then(resp => {
                console.log(`--- IMAGE HAS FILE SIZE: ${resp / 1024} Kb`);
                return {
                    valid: (resp / 1024 / 1024) <= this.maxSizeInMB,
                    sizeInMB: (resp / 1024 / 1024)
                };
            }, error => error);
    }

    presentAlert(message: string) {
        this.alertCtrl.create({
            header: 'Advertencia',
            message,
            buttons: [{ text: 'Ok' }],
            backdropDismiss: false
        }).then(a => a.present());
    }
}

export declare type PICKER_SOURCE_TYPE = 'camera' | 'imagePicker';
export const PICKER_SOURCE: { [key: string]: PICKER_SOURCE_TYPE } = { CAMERA: 'camera', IMAGE_PICKER: 'imagePicker' };
interface IMGVALIDATED {
    valid: boolean;
    dimensionResult: { valid: boolean, width: number, height: number, errors?: string };
    fileSizeResult: { valid: boolean, sizeInMB: number, errors?: string };
    uri: string;
}
