import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
    selector: 'app-upload-progress',
    templateUrl: './upload-progress.component.html',
    styleUrls: ['./upload-progress.component.scss'],
})
export class UploadProgressComponent implements OnInit {

    @Input() fileService: any;
    @Input() serverUrl: any;
    @Input() containerName: string;
    @Input() websiteId: number;
    @Input() AuthToken: string;
    @Input() images: string[];

    constructor(
        private popoverCtrl: PopoverController,
        private cdr: ChangeDetectorRef
    ) { }

    progressList = [];
    uploadStatus: { [key: number]: string[] } = {};

    response: { status: 'success' | 'error', uploadStatus?: { [key: number]: string[] } };

    ngOnInit(): void { }

    ionViewDidEnter() {
        const promises = [];

        this.images.forEach((imgUri, idx) => {
            promises[idx] = this.fileService.uploadFile(
                imgUri,
                this.serverUrl,
                // onProgress
                (progressEv) => {
                    this.progressList[idx] = Math.ceil(progressEv.loaded / progressEv.total * 100);
                    console.log(`--- this.progressList[${idx}]`, this.progressList[idx]);

                    this.cdr.detectChanges();
                },
                // uploader options
                {
                    fileName: `business.${this.containerName}_${idx}.jpg`,
                    fileKey: 'img',
                    headers: {
                        Authorization: `Bearer ${this.AuthToken}`
                    },
                    params: {
                        website_id: this.websiteId,
                        container: this.containerName.toLowerCase(),
                        index: idx
                    }
                })
                // on success
                .then((resp) => {
                    this.progressList[idx] = 100;
                    this.uploadStatus[idx] = ['ok'];
                    this.cdr.detectChanges();
                    return resp;
                }, (error) => {
                    let errors = ['Hubo un error desconocido'];

                    if (error.body) {
                        try {
                            errors = JSON.parse(error.body);
                            console.error('*** uploadImages ERROR:', errors);
                        }
                        catch (e) { }
                    }
                    else {
                        console.error('*** uploadImages ERROR:', error);
                    }

                    this.uploadStatus[idx] = errors;

                    return false;
                });
        });

        Promise.all(promises).then((resp: boolean[]) => {
            console.log('resp', resp);
            console.log('uploadStatus', this.uploadStatus);

            const hasErrors = resp.some(uploaded => {
                return uploaded !== true;
            });


            this.response = {
                status: hasErrors ? 'error' : 'success',
                uploadStatus: this.uploadStatus
            };

            this.popoverCtrl.dismiss(this.response);
        });
    }
}
