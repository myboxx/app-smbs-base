import { Component, OnDestroy, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { TOKEN_KEY } from '@boxx/login-core';
import { WebsitePageModel, WebsiteStore } from '@boxx/website-core';
import { ActionSheetController, AlertController, ModalController, PopoverController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FileNativeService } from 'src/services/NativeFile.service';
import { FirebasexService } from 'src/services/NativeFirebaseX.service';
import { LocalStorageNativeService } from 'src/services/NativeLocalStorage.service';
import { ImagePickerViewerComponent, PICKER_SOURCE_TYPE } from '../image-picker-viewer/image-picker-viewer.component';
import { UploadProgressComponent } from '../upload-progress/upload-progress.component';

declare type ImageDisplay = 'LOGO' | 'MAIN_BANNER' | 'GALLERY';

@Component({
    selector: 'app-website-images-page',
    templateUrl: './website-images-page.component.html',
    styleUrls: ['./website-images-page.component.scss'],
})
export class WebsiteImagesPageComponent implements OnInit, OnDestroy {

    constructor(
        public actionSheetController: ActionSheetController,
        private modalCtrl: ModalController,
        private popoverController: PopoverController,
        private alertCtrl: AlertController,
        private sanitizer: DomSanitizer,
        private store: WebsiteStore,
        private localStorageService: LocalStorageNativeService,
        private translate: TranslateService,
        private firebasex: FirebasexService,
        public toastController: ToastController,
        private fileService: FileNativeService
    ) {
        this.translate.get(['GENERAL', 'WEBSITE'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe(t => this.translations = t);
    }

    translations: any;

    readonly maxLogoDimensions = { width: 600, height: 300 };
    readonly maxMainDimensions = { width: 1749, height: 1849 };
    readonly maxGalleryDimensions = { width: 500, height: 500 };

    readonly MAX_GALLERY_IMAGES = 6;
    readonly MAX_GALLERY_ROWS = Math.round(this.MAX_GALLERY_IMAGES / 2);

    deleteFromContainer: ImageDisplay;
    deleteImageAtIndex = 0;

    businessImages = {
        businessLogo: '',
        businessMainImage: '',
        businessGallery: []
    };

    destroyed$ = new Subject<boolean>();
    websiteId: number;

    galleryErrors: { [key: number]: { url: string, errors: string[] } } = null;

    ngOnInit() {
        /*this.store.ImagesLoading$
          .pipe(takeUntil(this.destroyed$))
          .subscribe(loading => {
            console.log("WEBSITE IMAGES: LOADING...", loading);
          })*/

        this.store.Website$
            .pipe(takeUntil(this.destroyed$))
            .subscribe((websitePage: WebsitePageModel) => {
                if (!websitePage.websiteList[0].siteId) { return; }

                this.websiteId = websitePage.websiteList[0].siteId;

                // this.businessImages.businessLogo = websitePage.websiteList[0].logoUrl
            });

        this._handleSuccess();

        this._handleError();
    }

    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }

    ionViewDidEnter() {
        this.firebasex.setScreenName('WEBSITE > IMAGES');
    }

    private _handleSuccess() {
        this.store.Success$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(async (success) => {
                /*if (success && success.after == 'DELETE_IMAGE') {
                  const toast = await this.toastController.create({
                    message: this.translations.WEBSITE.SUCCESS[success.after.toLowerCase()],
                    duration: 3000,
                    color: 'success',
                  });
                  toast.present();

                  switch (this.deleteFromContainer) {
                    case 'LOGO':
                      this.businessImages.businessLogo = null;
                      break;
                    case 'MAIN_BANNER':
                      this.businessImages.businessMainImage = null;
                      break;
                    case 'GALLERY':
                      this.businessImages.businessGallery[this.deleteImageAtIndex] = null;
                  }
                }*/
            });
    }

    private _handleError() {
        this.store.Error$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(async (e) => {

                /*if (e && e.after == 'DELETE_IMAGE') {
                  const toast = await this.toastController.create({
                    message: this.translations.WEBSITE.ERRORS[e.after.toLowerCase()],
                    duration: 5000,
                    color: 'danger',
                  });
                  toast.present();

                  /*
                  for testing...
                  switch (this.deleteFromContainer) {
                    case 'LOGO':
                      this.businessImages.businessLogo = null;
                      break;
                    case 'MAIN_BANNER':
                      this.businessImages.businessMainImage = null;
                      break;
                    case 'GALLERY':
                      this.businessImages.businessGallery[this.deleteImageAtIndex] = null;
                  }
                }*/
            });
    }

    async selectImagesSource(container: ImageDisplay, galleryItem: number = 0) {
        const actionSheet = await this.actionSheetController.create({
            header: 'Seleccione',
            buttons: [{
                text: 'Albums',
                icon: 'images-outline',
                handler: () => {
                    this.openGallery(container, galleryItem);
                }
            }, {
                text: 'Cámara',
                icon: 'camera-outline',
                handler: () => {
                    this.openCamera(container, galleryItem);
                }
            }]
        });
        await actionSheet.present();
    }

    viewPhotos(data: { urls: string | string[], imagesErrors?: Array<string[]>, activeIndex?: number, forGallery?: boolean }) {
        this.modalCtrl.create({
            component: ImagePickerViewerComponent,
            componentProps: {
                images: typeof data.urls === 'object' ? data.urls : [data.urls],
                imagesErrors: data.imagesErrors || [[]],
                activeIndex: data.activeIndex || 0,
                forGallery: !!data.forGallery
            },
            swipeToClose: true
        }).then(m => m.present());
    }

    viewUploadErors() {
        const images = [];
        const imageErrors: Array<string[]> = [];
        Object.keys(this.galleryErrors)
            .forEach(key => {
                images.push(this.galleryErrors[key].url);
                imageErrors.push(this.galleryErrors[key].errors);
            });

        this.viewPhotos({
            urls: images,
            imagesErrors: imageErrors
        });
    }

    async openCamera(container: ImageDisplay, galleryItem: number = 0) {
        const source: PICKER_SOURCE_TYPE = 'camera';
        const modal = await this.modalCtrl.create({
            component: ImagePickerViewerComponent,
            componentProps: {
                source,
                showUploadBtn: true,
                maxDimensions: container === 'LOGO' ?
                    this.maxLogoDimensions : container === 'MAIN_BANNER' ? this.maxMainDimensions : this.maxGalleryDimensions,
                maxSizeInMB: 1
            }
        });

        await modal.present();

        const { data } = await modal.onDidDismiss();

        if (data) {
            this.uploadImages(data.images, container, galleryItem);
        }
    }

    async openGallery(container: ImageDisplay, galleryItem: number = 0) {
        const source: PICKER_SOURCE_TYPE = 'imagePicker';

        const modal = await this.modalCtrl.create({
            component: ImagePickerViewerComponent,
            componentProps: {
                source,
                showUploadBtn: true,
                maxImages: 1, // container != 'GALLERY' ? 1 : this.MAX_GALLERY_IMAGES - this.businessImages.businessGallery.length,
                maxDimensions: container === 'LOGO' ?
                this.maxLogoDimensions : container === 'MAIN_BANNER' ? this.maxMainDimensions : this.maxGalleryDimensions,
                maxSizeInMB: 1.0
            }
        });

        await modal.present();

        const { data } = await modal.onDidDismiss();

        if (data) {
            // if (container != 'GALLERY')
            this.uploadImages(data.images, container, galleryItem);
            /*else {
              this.businessImages.businessGallery[galleryItem] = data.images[0]
            }*/
        }
    }

    async uploadImages(images: string[], containerName: ImageDisplay, galleryItem: number = 0) {

        const popover = await this.popoverController.create({
            component: UploadProgressComponent,
            componentProps: {
                fileService: this.fileService,
                serverUrl: // 'https://vbandrew.000webhostapp.com/imageUploader/uploader.php',
                    // this.websiteRepo.getBaseUrl() + '/upload_update_images',
                    images,
                containerName,
                websiteId: this.websiteId,
                AuthToken: await this.localStorageService.getItem(TOKEN_KEY)
            },
            // event: ev,
            translucent: true,
            backdropDismiss: false
        });

        await popover.present();

        // let data : {status: 'success' | 'error', errors?: any};
        const { data } = await popover.onDidDismiss();

        console.log('--- DATA', data);

        if (data.status === 'success') {
            switch (containerName) {
                case 'LOGO':
                    this.businessImages.businessLogo = images[0]; break;
                case 'MAIN_BANNER':
                    this.businessImages.businessMainImage = images[0]; break;
                case 'GALLERY':
                    this.businessImages.businessGallery[galleryItem] = images[0];
            }

            console.log('this.businessImages.businessGallery', this.businessImages.businessGallery);

        }
        else {
            const uploadStatus: { [key: number]: string[] } = data.uploadStatus;
            let mainMsg = '';
            let causesMsg = '';

            if (images.length === 1) {
                mainMsg += `No se pudo subir la imagen<br>`;
                causesMsg += `- ${uploadStatus[0][0]}<br>`;
            }
            else if (containerName === 'GALLERY') {
                this.galleryErrors = {};

                let totalErrors = 0;
                Object.keys(uploadStatus).forEach(key => {
                    if (uploadStatus[key][0] === 'ok') {
                        this.businessImages.businessGallery = [...this.businessImages.businessGallery, images[key]];
                    }
                    else {
                        totalErrors++;
                        causesMsg += `<b>[Archivo ${(Number(key) + 1)}]</b><br>`;
                        uploadStatus[key].forEach((cause: string) => {
                            causesMsg += `- ${cause}<br>`;
                        });

                        this.galleryErrors[key] = { url: images[key], errors: uploadStatus[key] };
                    }
                });

                mainMsg = `No se pudieron subir ${totalErrors < images.length ? 'algunas' : 'las'} imágenes<br>`;
            }


            this.alertCtrl.create({
                header: 'Advertencia',
                message: mainMsg + causesMsg,
                backdropDismiss: false,
                buttons: [{ text: 'Ok' }]
            }).then(a => a.present());
        }
    }

    getSanitizedUrl(url: string) {
        if (!url) { return 'assets/icon/images.svg'; }

        return url.startsWith('file') ?
            (window as any).Ionic.WebView.convertFileSrc(url) :
            this.sanitizer.bypassSecurityTrustUrl(url);
    }

    async showImageOptions(container: ImageDisplay, index: number = 0) {
        const actionSheet = await this.actionSheetController.create({
            header: 'Acciones',
            buttons: [{
                text: 'Cambiar imagen (abrir galería)',
                icon: 'images-outline',
                handler: () => {
                    this.openGallery(container, index);
                }
            },
            {
                text: 'Cambiar imagen (tomar foto)',
                icon: 'camera-outline',
                handler: () => {
                    this.openCamera(container, index);
                }
            },
            {
                text: 'Eliminar',
                icon: 'trash-outline',
                role: 'destructive',
                handler: () => {
                    this.confirmDeleteImage(container, index);
                }
            }]
        });
        await actionSheet.present();
    }

    confirmDeleteImage(container: ImageDisplay, index: number = 0) {
        this.alertCtrl.create({
            header: 'Advertencia',
            message: `¿Realmente desea eliminar la imagen?<br><small>Esto acción no se puede deshacer</small>`,
            backdropDismiss: false,
            buttons: [
                {
                    text: 'Sí',
                    role: 'destructive',
                    handler: () => this.deleteImage(container, index)
                },
                { text: 'No' }
            ]
        }).then(a => a.present());
    }

    async deleteImage(container: ImageDisplay, index: number = 0) {
        /*this.deleteFromContainer = container;
        this.deleteImageAtIndex = index;

        let url_img = '';
        if(container == 'LOGO')
        url_img = this.businessImages.businessLogo
        else if(container == 'MAIN_BANNER')
        url_img = this.businessImages.businessMainImage
        else
        url_img = this.businessImages.businessGallery[index]

        const form: IDeleteImageFormProps = {
          website_id: this.websiteId,
          container: container.toLocaleLowerCase(),
          index,
          url_img
        }

        this.store.deleteImage(form)*/
    }
}
