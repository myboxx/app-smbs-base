import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IBusinessFormProps, WebsiteModel, WebsitePageModel, WebsiteStore } from '@boxx/website-core';
import { ToastController } from '@ionic/angular';
import { Observable, Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-website-products-page',
    templateUrl: './website-products.page.html',
    styleUrls: ['./website-products.page.scss']
})
export class WebsiteProductsPage implements OnInit, OnDestroy {
    @Input() enableEditModeSubject: Observable<boolean>;

    constructor(
        private store: WebsiteStore,
        private formBuilder: FormBuilder,
        public toastController: ToastController
    ) { }

    readonly MAX_PRODUCTS = 8;
    hoursOfOperation = [];

    private editModeSubscription: Subscription;
    destroyed$ = new Subject<boolean>();
    isEditing = false;

    originalProducts = [];
    productsForm: FormGroup;

    isLoading = false;
    selectedWebsite: WebsiteModel = WebsiteModel.empty();
    auxWebsitePage: WebsiteModel = null;

    // convenience getters for easy access to form fields
    get p() { return this.productsForm.controls.products as FormArray; }

    ngOnInit() {

        this.editModeSubscription = this.enableEditModeSubject.subscribe((enable) => {

            this.isEditing = enable;

            if (enable) {
                this.productsForm.enable();
            }
            else {
                this._initProductsForm(this.auxWebsitePage);
                this.productsForm.disable();
            }
        });

        this.productsForm = this.formBuilder.group({
            products: new FormArray([]),
        });

        this.store.Loading$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(loading => this.isLoading = loading);

        this.store.Website$
            .pipe(takeUntil(this.destroyed$))
            .subscribe((websitePage: WebsitePageModel) => {
                if (!websitePage.websiteList[0].siteId) { return; }

                this.selectedWebsite = websitePage.websiteList[0];
                this.auxWebsitePage = websitePage.websiteList[0]; // to restore values if close btn is tapped

                this._initProductsForm(this.selectedWebsite);

                this.hoursOfOperation = websitePage.websiteList[0].hoursOfOperation;
            });

        this.store.HasBeenFetched$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(ok => {
                if (!ok) {
                    this.store.LoadWebsite();
                }
            });
    }

    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
        this.editModeSubscription.unsubscribe();
    }

    private _initProductsForm(selectedWebsite: WebsiteModel) {
        this.productsForm = this.formBuilder.group({
            products: new FormArray([]),
        });

        selectedWebsite.products.forEach((p) => {
            this.addProductField(p);
        });

        this.productsForm.disable();

        this.isEditing = false;
    }

    addProductField(val: string = '') {
        this.p.push(this.formBuilder.group({
            description: [val, Validators.required],
        }));
    }

    async removeProduct(idx: number) {
        if (this.p.length > 1) {
            this.p.removeAt(idx);
        }
        else {
            const toast = await this.toastController.create({
                message: 'Al menos debes conservar 1 producto',
                duration: 5000,
                color: 'warning',
            });
            toast.present();
        }
    }

    canUpdateProds(): boolean {
        return this.p.controls.length > 0 && !this.p.controls.some(c => c.invalid);
    }

    updateBusiness() {
        const products = [];
        this.p.controls.forEach((c) => products.push(c.value.description));

        const businessForm: IBusinessFormProps = {
            products,
            payment_forms: [
                'visa',
                'mastercard'
            ],
            hours_of_operation: this.hoursOfOperation,
            hours_of_operation_notes: 'Cerramos dias festivos'
        };

        this.store.UpdateBusinessData(this.selectedWebsite.siteId, businessForm);

    }
}
