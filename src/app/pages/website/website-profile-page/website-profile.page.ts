import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { WebsiteModel, WebsitePageModel, WebsiteStore } from '@boxx/website-core';
import { ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FirebasexService } from 'src/services/NativeFirebaseX.service';
import { SharingService } from 'src/services/NativeSharing.service';
import { WebsiteUpdateModalPage } from '../website-update-modal/website-update.modal';

@Component({
    selector: 'app-website-profile-page',
    templateUrl: './website-profile.page.html',
    styleUrls: ['./website-profile.page.scss']
})
export class WebsiteProfilePage implements OnInit, OnDestroy {
    @Input() openModalSubject: Observable<boolean>;

    constructor(
        private store: WebsiteStore,
        private modalCtrl: ModalController,
        private firebasex: FirebasexService,
        private sharingSrvc: SharingService,
        private translate: TranslateService
    ) {
        this.translate.get(['GENERAL', 'WEBSITE'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe(t => this.translations = t);
    }

    private eventsSubscription: Subscription;


    destroyed$ = new Subject<boolean>();
    selectedWebsite: WebsiteModel = WebsiteModel.empty();
    auxWebsitePage: WebsiteModel = null;

    websiteForm: FormGroup;
    isLoading = false;

    translations: any;

    ngOnInit() {
        this.eventsSubscription = this.openModalSubject.subscribe((open) => {
            if (open) { this.presentModal(); }
        });

        this.store.Loading$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(loading => this.isLoading = loading);

        this.websiteForm = new FormGroup({
            title: new FormControl(),
            email: new FormControl(),
            facebook_url: new FormControl(),
            twitter_url: new FormControl(),
            instagram_url: new FormControl(),
            gplus: new FormControl(),
            about: new FormControl(),
            phone: new FormControl(),
            mobile: new FormControl(),
        });
        this.websiteForm.disable();

        this.store.Website$
            .pipe(takeUntil(this.destroyed$))
            .subscribe((websitePage: WebsitePageModel) => {
                if (!websitePage.websiteList[0].siteId) { return; }

                this.selectedWebsite = websitePage.websiteList[0];
                this.auxWebsitePage = websitePage.websiteList[0]; // to restore values if close btn is tapped

                this.websiteForm.patchValue({
                    title: this.selectedWebsite.title,
                    email: this.selectedWebsite.email,
                    facebook_url: this.selectedWebsite.facebook,
                    twitter_url: this.selectedWebsite.twitter,
                    instagram_url: this.selectedWebsite.instagram,
                    gplus: this.selectedWebsite.gplus,
                    about: this.selectedWebsite.about,
                    phone: this.selectedWebsite.phone,
                    mobile: this.selectedWebsite.mobile
                });
            });

        this.store.HasBeenFetched$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(ok => {
                if (!ok) {
                    this.store.LoadWebsite();
                }
            });
    }

    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
        this.eventsSubscription.unsubscribe();
    }

    async presentModal() {

        const modal = await this.modalCtrl.create({
            component: WebsiteUpdateModalPage,
            componentProps: {
                websiteId: this.selectedWebsite.siteId,
                websiteForm: this.websiteForm.value
            }
        });

        this.firebasex.setScreenName('WEBSITE > UPDATE');

        await modal.present();

        const { data } = await modal.onWillDismiss();
    }


    openWebsite() {
        this.sharingSrvc.openURL(this.selectedWebsite.siteUrl);
    }

    shareWebsite() {
        this.sharingSrvc.socialSharing({
            message: this.translations.WEBSITE.visitMyWebsite,
            subject: '',
            url: this.selectedWebsite.siteUrl,
            chooserTitle: this.translations.GENERAL.share
        });
    }
}
