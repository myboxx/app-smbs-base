import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { IBusinessFormProps, WebsiteModel, WebsitePageModel, WebsiteStore } from '@boxx/website-core';
import { Observable, Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

declare type BusinessScheduleType = 'HOURS' | '24_HOURS' | 'NO_HOURS';

@Component({
    selector: 'app-website-schedule-page',
    templateUrl: './website-schedule-page.html',
    styleUrls: ['./website-schedule-page.scss']
})
export class WebsiteSchedulePage implements OnInit, OnDestroy {
    @Input() enableEditModeSubject: Observable<boolean>;

    constructor(
        private store: WebsiteStore,
        private formBuilder: FormBuilder
    ) { }


    private editModeSubscription: Subscription;
    isEditing = false;

    products = [];
    days = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];

    scheduleType: BusinessScheduleType = 'HOURS';

    isLoading = false;
    selectedWebsite: WebsiteModel = WebsiteModel.empty();
    auxWebsitePage: WebsiteModel = null;
    destroyed$ = new Subject<boolean>();

    scheduleForm: FormGroup;
    // convenience getters for easy access to form fields
    get d() { return this.scheduleForm.controls.schedule as FormArray; }

    ngOnInit() {
        this.editModeSubscription = this.enableEditModeSubject.subscribe((enable) => {

            this.isEditing = enable;

            if (enable) {
                this.scheduleForm.enable();

                const dayControlArray: Array<any> = (this.scheduleForm.controls as any).schedule.controls;

                dayControlArray.forEach((ctrl, i) => {
                    if (ctrl.controls[`closed${i}`].value === true) {
                        ctrl.controls[`from${i}`].disable();
                        ctrl.controls[`to${i}`].disable();
                    }
                });
            }
            else {
                this.scheduleForm.controls.type.disable();
                this._initScheduleForm(this.auxWebsitePage);
                this.scheduleForm.disable();
            }
        });

        this.scheduleForm = this.formBuilder.group({
            type: new FormControl({ value: 'NO_HOURS', disabled: true }),
            schedule: new FormArray([]),
            description: new FormControl(''),
            extraInfo: new FormControl('')
        });

        this.store.Website$
            .pipe(takeUntil(this.destroyed$))
            .subscribe((websitePage: WebsitePageModel) => {
                if (!websitePage.websiteList[0].siteId) { return; }

                this.selectedWebsite = websitePage.websiteList[0];
                this.auxWebsitePage = websitePage.websiteList[0]; // to restore values if close btn is tapped

                this._initScheduleForm(this.selectedWebsite);

                this.products = websitePage.websiteList[0].products;
            });

        this.store.HasBeenFetched$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(ok => {
                if (!ok) {
                    this.store.LoadWebsite();
                }
            });
    }

    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
        this.editModeSubscription.unsubscribe();
    }

    private _initScheduleForm(selectedWebsite: WebsiteModel) {

        this.scheduleForm = this.formBuilder.group({
            type: this.calcScheduleType(selectedWebsite.hoursOfOperation),
            schedule: this._createScheduleGroup(selectedWebsite.hoursOfOperation),
            description: selectedWebsite.hoursOfOperationNotes,
            extraInfo: new FormControl(''),
        });

        this.scheduleForm.disable();
    }

    private calcScheduleType(hoursOfOperation: Array<any>): BusinessScheduleType {
        let type: BusinessScheduleType = 'NO_HOURS';

        if (hoursOfOperation.length === 0) {
            type = 'NO_HOURS';
        }
        else if (hoursOfOperation[0] === '24_HOURS') {
            type = '24_HOURS';
        }
        else {
            type = 'HOURS';
        }

        this.scheduleType = type;

        return type;
    }

    private _createScheduleGroup(hours: Array<any>) {
        const rows = [];

        if (hours.length === 7) {
            hours.forEach((h, i) => {

                const r = this.formBuilder.group({
                    [`from${i}`]: [h.length ? h[0][0] : '00:00', Validators.required],
                    [`to${i}`]: [h.length ? h[0][1] : '00:00', Validators.required],
                    [`closed${i}`]: [h.length === 0]
                });

                if (h.length !== 0) {
                    if ((h[0][0] as string).startsWith('00:00') && (h[0][1] as string).startsWith('00:00')) {
                        r.controls[`from${i}`].disable();
                        r.controls[`to${i}`].disable();
                        r.controls[`closed${i}`].patchValue(true);
                    }
                    else {
                        r.controls[`from${i}`].enable();
                        r.controls[`to${i}`].enable();
                    }
                }
                else {
                    r.controls[`from${i}`].disable();
                    r.controls[`to${i}`].disable();
                }

                rows.push(r);
            });
        }
        else {
            for (let i = 0; i < 7; i++) {
                rows.push(
                    this.formBuilder.group({
                        [`from${i}`]: ['08:00', Validators.required],
                        [`to${i}`]: ['17:00', Validators.required],
                        [`closed${i}`]: false
                    })
                );
            }
        }

        return new FormArray(rows);
    }

    toggleClosedOn(index: number) {

        const ctrls = (this.d.controls[index] as FormArray).controls;

        // because checkbox's value is not synchronized (is not the actual value)
        if (ctrls[`closed${index}`].value === true) {
            this.d.controls[index].patchValue({
                [`from${index}`]: '08:00',
                [`to${index}`]: '17:00',
            });

            ctrls[`from${index}`].enable();
            ctrls[`to${index}`].enable();
        }
        else {
            this.d.controls[index].patchValue({
                [`from${index}`]: '00:00',
                [`to${index}`]: '00:00',
            });

            ctrls[`from${index}`].disable();
            ctrls[`to${index}`].disable();
        }
    }

    onHoursChanged(index: number, input: any) {
        const ctrls = this.d.controls[index];

        if (input[`from${index}`] === input[`to${index}`]) {
            ctrls.patchValue({ [`closed${index}`]: true });
            (ctrls as FormArray).controls[`from${index}`].disable();
            (ctrls as FormArray).controls[`to${index}`].disable();
        }
    }

    applyToAll() {
        if (this.scheduleForm.disabled) { return; }

        this.d.controls.forEach((c: any, idx) => {
            c.patchValue({
                [`from${idx}`]: this.d.controls[0].value.from0,
                [`to${idx}`]: this.d.controls[0].value.to0,
                [`closed${idx}`]: this.d.controls[0].value.closed0,
            });

            if (this.d.controls[0].value.closed0) {
                c.controls[`from${idx}`].disable();
                c.controls[`to${idx}`].disable();
            }
            else {
                c.controls[`from${idx}`].enable();
                c.controls[`to${idx}`].enable();
            }
        });
    }

    onScheduleTypeChanged(value: BusinessScheduleType) {
        this.scheduleType = value;
    }

    updateBusiness() {
        let hoursOfOperation = [];

        switch ((this.scheduleForm.controls.type.value as BusinessScheduleType)) {
            case '24_HOURS':
                hoursOfOperation = ['24_HOURS'];
                break;
            case 'HOURS':
                this.d.controls.forEach((ctrlGroup: any, i) => {
                    if (ctrlGroup.controls[`closed${i}`].value === true) {
                        hoursOfOperation.push([]);
                    }
                    else {
                        hoursOfOperation.push(
                            ctrlGroup.controls[`from${i}`].value !== ctrlGroup.controls[`to${i}`].value ?
                                [[
                                    (ctrlGroup.controls[`from${i}`].value + ':00').substring(0, 8),
                                    (ctrlGroup.controls[`to${i}`].value + ':00').substring(0, 8)]
                                ] :
                                []
                        );
                    }
                });
                break;
            default:
                hoursOfOperation = [];
        }

        const businessForm: IBusinessFormProps = {
            products: this.products,
            payment_forms: [
                'visa',
                'mastercard'
            ],
            hours_of_operation: hoursOfOperation,
            hours_of_operation_notes: 'Cerramos dias festivos'
        };

        this.store.UpdateBusinessData(this.selectedWebsite.siteId, businessForm);

    }
}
