import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { WebsiteStore } from '@boxx/website-core';
import { ModalController } from '@ionic/angular';
import { APP_CONSTANTS } from 'src/constants/constants';

@Component({
    selector: 'app-website-update-modal',
    templateUrl: './website-update.modal.html',
    styleUrls: ['./website-update.modal.scss']
})
export class WebsiteUpdateModalPage implements OnInit {

    constructor(
        private modalCtrl: ModalController,
        private store: WebsiteStore
    ) {

    }
    @Input() websiteId: number;
    @Input() websiteForm: {
        title: string,
        email: string,
        facebook_url: string,
        twitter_url: string,
        instagram_url: string,
        gplus: string,
        about: string,
        phone: string,
        mobile: string
    };

    websiteUpdateForm: FormGroup;

    validationMessages = {
        title: [
            { type: 'required', message: 'La descripción es requerida' },
        ],
        email: [
            { type: 'required', message: '•El email es requerido' },
            { type: 'pattern', message: '•Formato de email incorrecto' }
        ],
        phone: [
            { type: 'required', message: '•El teléfono es requerido' },
            { type: 'pattern', message: '•Sólo debe contener números' },
            { type: 'minlength', message: 'Deben ser exactamente 10 dígitos' },
            { type: 'maxlength', message: 'Deben ser exactamente 10 dígitos' },
        ],
        mobile: [
            { type: 'required', message: '•El teléfono móvil es requerido' },
            { type: 'pattern', message: '•Sólo debe contener números' },
            { type: 'minlength', message: 'Deben ser exactamente 10 dígitos' },
            { type: 'maxlength', message: 'Deben ser exactamente 10 dígitos' },
        ],
    };

    ngOnInit() {
        this.websiteUpdateForm = new FormGroup({
            title: new FormControl('', [Validators.required]),
            email: new FormControl('', [Validators.required, Validators.pattern(APP_CONSTANTS.EMAILPATTERN)]),
            facebook_url: new FormControl(),
            twitter_url: new FormControl(),
            instagram_url: new FormControl(),
            gplus: new FormControl(),
            about: new FormControl(),
            phone: new FormControl(
                '', [Validators.minLength(10), Validators.maxLength(10), Validators.required, Validators.pattern('[0-9]*')]
            ),
            mobile: new FormControl('', [Validators.minLength(10), Validators.maxLength(10), Validators.pattern('[0-9]*')]),
        });
    }

    ionViewDidEnter() {
        this.websiteUpdateForm.patchValue(this.websiteForm);
        this.websiteUpdateForm.get('title').disable();
    }

    update() {
        this.store.UpdateWebsite(this.websiteId, this.websiteUpdateForm.value);
        this.modalCtrl.dismiss(/*{someData: ...} */);
    }

    close() {
        this.modalCtrl.dismiss(/*{someData: ...} */);
    }
}
