import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { ImagePickerViewerComponent } from './image-picker-viewer/image-picker-viewer.component';
import { UploadProgressComponent } from './upload-progress/upload-progress.component';
import { WebsiteImagesPageComponent } from './website-images-page/website-images-page.component';
import { WebsiteProductsPage } from './website-products-page/website-products.page';
import { WebsiteProfilePage } from './website-profile-page/website-profile.page';
import { WebsitePageRoutingModule } from './website-routing.module';
import { WebsiteSchedulePage } from './website-schedule-page/website-schedule-page';
import { WebsiteUpdateModalPage } from './website-update-modal/website-update.modal';
import { WebsitePage } from './website.page';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        IonicModule,
        WebsitePageRoutingModule,
        TranslateModule.forChild(),
        PipesModule
    ],
    declarations: [
        WebsitePage,
        WebsiteUpdateModalPage,
        UploadProgressComponent,
        WebsiteProfilePage,
        WebsiteSchedulePage,
        WebsiteProductsPage,
        WebsiteImagesPageComponent,
        ImagePickerViewerComponent
    ]
})
export class WebsitePageModule { }
