import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WebsiteModel, WebsitePageModel, WebsiteStore } from '@boxx/website-core';
import { ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FirebasexService } from 'src/services/NativeFirebaseX.service';

declare type WebsiteActiveSegment = 'PROFILE' | 'SCHEDULE' | 'SERVICES' | 'STATS' | 'IMAGES';

@Component({
    selector: 'app-website',
    templateUrl: './website.page.html',
    styleUrls: ['./website.page.scss'],
})
export class WebsitePage implements OnInit, OnDestroy {

    constructor(
        private store: WebsiteStore,
        private translate: TranslateService,
        private router: Router,
        private route: ActivatedRoute,
        public toastController: ToastController,
        private firebasex: FirebasexService
    ) {
        this.translate.get(['GENERAL', 'WEBSITE'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe(t => this.translations = t);

        this.route.queryParams
            .pipe(takeUntil(this.destroyed$))
            .subscribe(params => {
                if (this.router.getCurrentNavigation().extras.state) {
                    this.activeSegment = ((this.router.getCurrentNavigation().extras.state.type || 'PROFILE') as WebsiteActiveSegment);
                }
                else {
                    this.activeSegment = 'PROFILE';
                }

                this.isEditing = false;
            });
    }

    activeSegment: WebsiteActiveSegment = 'PROFILE';

    isLoading = false;
    isEditing = false;

    selectedWebsite: WebsiteModel = WebsiteModel.empty();
    destroyed$ = new Subject<boolean>();

    translations: any;

    editProfileSubject: Subject<boolean> = new Subject<boolean>();
    enableEditModeSubject: Subject<boolean> = new Subject<boolean>();

    ngOnInit() {
        this.store.Loading$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(loading => this.isLoading = loading);

        this.store.Website$
            .pipe(takeUntil(this.destroyed$))
            .subscribe((websitePage: WebsitePageModel) => {
                if (!websitePage.websiteList[0].siteId) { return; }

                this.selectedWebsite = websitePage.websiteList[0];
            });

        this._handleSuccess();

        this._handleError();
    }

    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }

    ionViewDidEnter() {
        this.firebasex.setScreenName('WEBSITE');
    }

    private _handleSuccess() {
        this.store.Success$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(async (success) => {
                if (success && success.after !== 'GET') {
                    const toast = await this.toastController.create({
                        message: this.translations.WEBSITE.SUCCESS[success.after.toLowerCase()],
                        duration: 3000,
                        color: 'success',
                    });
                    toast.present();

                    this.enableEdition(false);
                }
            });
    }

    private _handleError() {
        this.store.Error$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(async (e) => {

                if (e) {
                    const toast = await this.toastController.create({
                        message: this.translations.WEBSITE.ERRORS[e.after.toLowerCase()],
                        duration: 5000,
                        color: 'danger',
                    });
                    toast.present();

                    this.enableEdition(false);
                }
            });
    }

    segmentChanged(value: WebsiteActiveSegment) {
        this.activeSegment = value;
    }

    enableEdition(enable: boolean) {
        // don't activate editing btn in PROFILE!!!
        if (this.activeSegment === 'PROFILE') {
            return this.editProfileSubject.next(enable);
        }

        this.isEditing = enable;

        this.enableEditModeSubject.next(enable);
    }

    calcFontSize(views: number) {
        return 64 - 10 * (views / 100 < 1 ? 0 : views / 1000 < 1 ? 1 : views / 10000 < 1 ? 2 : views / 10000 < 1 ? 3 : 4);
    }
}
