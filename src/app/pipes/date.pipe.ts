import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

/**
 * Takes a number and transforms into percentage upto
 * specified decimal places
 *
 * Usage:
 * value | percent-pipe: decimalPlaces
 *
 * Example:
 * 0.1335 | percent-pipe:2
 */

@Pipe({
    name: 'myDate'
})
export class DatePipe implements PipeTransform {
    /**
     * Takes a number and returns percentage value
     * @param value: number
     * @param decimalPlaces: number
     */
    transform(value: string, format: string) {
        return moment(value).format(format);
    }
}
