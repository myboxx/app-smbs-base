import { NgModule } from '@angular/core';
import { DatePipe } from '../pipes/date.pipe';
import { ReplacePipe } from '../pipes/replace.pipe';
import { ZeroPaddingPipe } from '../pipes/zero-padding.pipe';

@NgModule({
    imports: [
    ],
    declarations: [
        ReplacePipe,
        DatePipe,
        ZeroPaddingPipe
    ],
    exports: [
        ReplacePipe,
        DatePipe,
        ZeroPaddingPipe
    ]
})

export class PipesModule { }
