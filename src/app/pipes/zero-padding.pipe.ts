import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'zeroPadding'
})
export class ZeroPaddingPipe implements PipeTransform {

    transform(value: number, char: string = '0'): any {
        return value > 0 && value < 10 ? `${char}${value}` : value;
    }
}
