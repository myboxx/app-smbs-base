export const APP_CONSTANTS = {
    PRODUCT_PAGE_URL: 'https://www.concanaco.com.mx/',
    SUPPORT_EMAIL_ADDRESS: 'mpdconcanaco.soporte@virket2593.zohodesk.com',
    SUPPORT_PHONE: '55 53 50 87 01',
    SUPPORT_SUBJECT: 'Soporte Espacio Emprendedor',
    PRIVACY_POLICY_URL: 'http://mpd.concanaco.com.mx/aviso-de-privacidad/-/-/-/',
    TERMS_AND_CONDITIONS_URL: 'http://mpd.concanaco.com.mx/terminos-y-condiciones/-/-/-/',
    BLOG_URL: 'http://blog.getnet.store/',
    EMAILPATTERN: /^[a-z0-9!#$%&'*\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])+)+$/i,
    ONLY_LETTERS_PATTERN: /^[a-zA-ZÀ-ȕ ]+$/,
};
