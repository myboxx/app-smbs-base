import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Injectable } from '@angular/core';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker/ngx';
import { Crop, CropOptions } from '@ionic-native/crop/ngx';

@Injectable({
    providedIn: 'root'
})
export class CameraNativeService {

    constructor(
        private camera: Camera,
        private imagePicker: ImagePicker,
        private crop: Crop
    ) { }

    openImagePicker(customOptions?: ImagePickerOptions): Promise<any> {
        const defaultOptions: ImagePickerOptions = {
            // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
            // selection of a single image, the plugin will return it.
            maximumImagesCount: 6,

            // max width and height to allow the images to be.  Will keep aspect
            // ratio no matter what.  So if both are 800, the returned image
            // will be at most 800 pixels wide and 800 pixels tall.  If the width is
            // 800 and height 0 the image will be 800 pixels wide if the source
            // is at least that wide.
            // width: int,
            // height: int,

            // quality of resized image, defaults to 100
            quality: 100,

            // output type, defaults to FILE_URIs.
            // available options are
            // window.imagePicker.OutputType.FILE_URI (0) or
            // window.imagePicker.OutputType.BASE64_STRING (1)
            outputType: 0
        };

        const self = this;
        const openPicker = (): Promise<any> => new Promise((resolve, reject) => {
            self.imagePicker.getPictures({
                ...defaultOptions,
                ...customOptions
            }).then(resp => {
                resolve(resp);
            }, error => {
                console.error('**** getPictures error:', error);
                reject(error);
            });
        });

        return new Promise((resolve, reject) => {
            this.imagePicker.hasReadPermission().then(permission => {
                // console.log("--- hasReadPermission", permission);

                if (!permission) {
                    this.imagePicker.requestReadPermission().then(thePermission => {
                        // console.log("--- requestReadPermission", permission);
                        if (!thePermission) { reject('Se requiere permiso para abrir la galería'); }
                        else { resolve(openPicker()); }
                    }, error => {
                        console.error('*** requestReadPermission error:', error);
                        reject(error);
                    });
                }
                else {
                    resolve(openPicker());
                }

            }, error => {
                console.error('*** hasReadPermission error:', error);
                reject(error);
            });
        });

    }

    openCameraMedia(source: 'camera' | 'gallery', customOptions: CameraOptions = {}): Promise<any> {
        const src = source === 'camera' ? 'CAMERA' : 'SAVEDPHOTOALBUM';

        const defaultOptions: CameraOptions = {
            sourceType: this.camera.PictureSourceType[src],
            destinationType: this.camera.DestinationType.DATA_URL,
            mediaType: 0, // picture only
            quality: 70,
            targetWidth: 300,
            targetHeight: 300,
            allowEdit: false,
            correctOrientation: true,
            encodingType: 1 // JPEG : 0 , PNG : 1
        };

        return new Promise((resolve, reject) => {
            this.camera.getPicture({
                ...defaultOptions,
                ...customOptions
            }).then(data => {
                console.log('---- on getPicture resolved!');

                // see cleanup() method
                if (
                    defaultOptions.sourceType === this.camera.PictureSourceType.CAMERA &&
                    defaultOptions.destinationType === this.camera.DestinationType.FILE_URI
                ) {
                    console.log('---- Cleaning up camera data');
                    this.camera.cleanup();
                }

                resolve(data);
            }, error => {
                console.error('*** openCameraMedia()->getPicture() error:', error);
                reject(error);
            });
        });
    }

    cropImage(imgUri: string, cropOtps: CropOptions = {}): Promise<string> {
        return new Promise((resolve, reject) => {
            const defaultOtps = {
                quality: 100
            };

            this.crop.crop(imgUri, { ...defaultOtps, ...cropOtps }).then(resp => {
                resolve(resp);
            }).catch(error => {
                console.error('*** crop error:', error);
                reject(error);
            });
        });
    }
}
