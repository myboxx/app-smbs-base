import { File } from '@ionic-native/file/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class FileNativeService {
    constructor(
        private file: File,
        private transfer: FileTransfer
    ) { }

    readonly fileTransfer: FileTransferObject = this.transfer.create();

    getImgFileSize(imageUri: string): Promise<number> {
        return new Promise((resolve, reject) => {
            if (imageUri.startsWith('data:image')) {
                const contentWithoutMime = imageUri.split(',')[1];
                const sizeInBytes = window.atob(contentWithoutMime).length;
                return resolve(sizeInBytes);
            }

            this.file.resolveLocalFilesystemUrl(imageUri).then(fileEntry => {
                fileEntry.getMetadata(fileObj => {
                    const size = fileObj.size;
                    resolve(size);
                }, e => {
                    console.error('**** getImgFileSize() -> fileEntry.getMetadata()', e);
                    reject(e);
                });
            }).catch(e => {
                console.error('**** getImgFileSize() -> file.resolveLocalFilesystemUrl()', e);
                reject(e);
            });
        });
    }

    getImgDimensions(imageUri: string): Promise<{ width: number, height: number }> {
        // imageUri = normalizeUrl(imageUri);
        return new Promise((resolve, reject) => {
            let img = new Image();

            img.onload = (_) => {
                resolve({ width: img.width, height: img.height });
                img = null;
            };

            img.onerror = (e) => {
                reject(e);
                img = null;
            };

            img.src = imageUri;
        });
    }

    uploadFile(
        fileUrl: string,
        server: string,
        onProgressEvent: (event: ProgressEvent) => void, options?: FileUploadOptions
    ): Promise<boolean> {
        const defaultOptions: FileUploadOptions = {
            fileKey: 'file',
            fileName: `image.png`,
            headers: {
                // Authorization: `Bearer ${token}`
                Accept: '*/*'
            },
            params: { submit: 'submit' }
        };

        return new Promise((resolve, reject) => {
            this.fileTransfer.upload(fileUrl, server, { ...defaultOptions, ...options }).then(
                (result) => {
                    console.log('--- File upload result', result);
                    resolve(true);
                },
                (error) => {
                    console.error('**** uploadFile() ERROR:', error);
                    reject(error);
                }
            );

            this.fileTransfer.onProgress((p) => {
                if (onProgressEvent) {
                    onProgressEvent(p);
                }
            });

        });
    }

    imgDataUriToBase64(imageUri: string, outputFormat: string = 'image/jpeg'): Promise<string> {
        return new Promise((resolve, reject) => {
            if (!imageUri) {
                return reject('Image URI must not be null/empty');
            }
            if (!outputFormat) {
                return reject('Image output format must not be null/empty');
            }

            let img = new Image();
            img.crossOrigin = 'Anonymous';

            img.onload = () => {
                let c = document.createElement('canvas') as HTMLCanvasElement;
                const ctx = c.getContext('2d');
                c.width = img.width;
                c.height = img.height;
                ctx.drawImage(img, 0, 0);
                const dataURL = c.toDataURL(outputFormat);
                c = null;
                resolve(dataURL);
                img = null;
            };

            img.onerror = (e) => {
                console.error('*** imgDataUriToBase64 ERROR:', e);

                reject(e);
                img = null;
            };

            img.src = ( window as any).Ionic.WebView.convertFileSrc(imageUri);
        });
    }
}
