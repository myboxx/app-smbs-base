import { Injectable, NgZone } from '@angular/core';
import { FirebaseX } from '@ionic-native/firebase-x';

@Injectable({
    providedIn: 'root'
})
export class FirebasexService {
    constructor(private zone: NgZone) { }

    setAnalyticsCollectionEnabled(val: boolean) {
        return FirebaseX.setAnalyticsCollectionEnabled(val);
    }

    setCrashlyticsCollectionEnabled(val: boolean) {
        return FirebaseX.setCrashlyticsCollectionEnabled(val);
    }

    getToken() {
        return FirebaseX.getToken();
    }

    getAPNSToken() {
        return FirebaseX.getAPNSToken();
    }

    onMessageReceived() {
        return FirebaseX.onMessageReceived();
    }

    setScreenName(name: string) {
        console.log('--- setScreenName', name);

        return this.zone.runTask(() => {
            return FirebaseX.setScreenName(name);
        });
    }
}
