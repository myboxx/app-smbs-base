import { Injectable } from '@angular/core';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { IEventReminder } from '@boxx/events-core';

@Injectable({
    providedIn: 'root'
})
export class LocalNotificationsService {

    constructor(
        private localNotifications: LocalNotifications
    ) { }

    private hasPermission = false;

    setReminder(reminder: IEventReminder) {
        const today = new Date();
        if (reminder.date > today) {
            this.localNotifications.schedule({
                id: reminder.id,
                title: reminder.title,
                text: reminder.text,
                trigger: { at: reminder.date }
            });
            console.log('--- recordatorio creado correctamente para: ', reminder.date);
        }
    }

    removeReminder(id: number) {
        return new Promise((resolve, reject) => {
            return this.localNotifications.cancel(id)
                .then(() => {
                    console.log('--- notification  OK');
                    resolve(true);
                }, error => {
                    console.error('--- removeReminder', error);
                    reject(error);
                })
                .catch((err) => { reject(err); });
        });
    }

    updateReminder(reminder: IEventReminder) {
        const today = new Date();
        if (reminder.date > today) {
            this.localNotifications.clear(reminder.id)
                .then(() => this.setReminder(reminder))
                .catch(err => console.warn('Reminder not cleared', err));
        }
    }

    checkIdScheduled(id: number) {
        this.localNotifications.isScheduled(id).then((scheduled) => {
            console.log('isScheduled', scheduled);
        });
    }

    init() {
        return new Promise(async (resolve, reject) => {
            await this.localNotifications.hasPermission().then((granted) => {
                if (!granted) {
                    console.warn('*** Requesting permission for Local Notifications...');

                    this.localNotifications.requestPermission().then(r => {
                        console.log('--- Local notifications permission: ', r);

                        this.hasPermission = r;
                    });
                }
                else {
                    this.hasPermission = true;
                }
            }, error => {
                console.error('*** localNotifications.hasPermission()', error);

                return reject(error);
            });

            resolve(this.hasPermission);

            if (this.hasPermission) {
                console.log('--- Local notifications has permissions!!!');

                this.localNotifications.fireQueuedEvents()
                    .then(
                        resp => console.log('--- fireQueuedEvents OK', resp),
                        error => console.error('--- fireQueuedEvents ERROR', error)
                    ).catch(e => console.error('--- CATCH: fireQueuedEvents', e));
            }
        });
    }

    stop() {
        this.localNotifications.cancelAll().then((res) => {
            console.log('--- localNotifications.cancelAll()', res);
            this.localNotifications.clearAll().then((res2) => {
                console.log('--- localNotifications.clearAll()', res2);
            });
        });
    }
}
