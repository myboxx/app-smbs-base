import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { INativeStorageService } from '@boxx/login-core';

@Injectable({
    providedIn: 'root'
})
export class LocalStorageNativeService implements INativeStorageService {

    constructor(
        private plt: Platform, private stg: NativeStorage
    ) {
        this.storage = this.plt.is('cordova' || 'capacitor') ? stg : localStorage;
    }

    private storage: any;

    getItem(key: string): Promise<string> {
        return new Promise((resolve, reject) => {
            if (this.plt.is('cordova' || 'capacitor')) {
                (this.storage as NativeStorage)
                    .getItem(key).then(
                        (val) => resolve(val),
                        e => {
                            if (e.code && e.code === 2) {
                                // Key is not present in storage
                                resolve(null);
                            }
                            else {
                                console.error(`REJECT: ERROR while trying to get key ${key} from localstore`);
                                reject(e);
                            }
                        }
                    ).catch(e => {
                        if (e.code && e.code === 2) {
                            // Key is not present in storage
                            resolve(null);
                        }
                        else {
                            console.error(`CATCH: ERROR while trying to get key ${key} from localstore`);
                            reject(e);
                        }
                    });
            }
            else {
                resolve(this.storage.getItem(key));
            }
        });
    }

    setItem(key: string, value: any): Promise<any> {
        return Promise.resolve(this.storage.setItem(key, value));
    }

    removeItem(key: string): Promise<any> {
        return this.plt.is('cordova' || 'capacitor') ?
            (this.storage as NativeStorage).remove(key) :
            Promise.resolve(this.storage.removeItem(key));
    }

    keys(): Promise<Array<string>> {
        if (this.plt.is('cordova' || 'capacitor')) {
            return this.stg.keys();
        }

        const keys: Array<string> = [];

        for (let i = 0; i < localStorage.length; i++) { keys.push(localStorage.key(i)); }

        return Promise.resolve(keys);
    }

    clearStorage(): Promise<any> {
        return Promise.resolve(this.storage.clear());
    }
}

@Injectable()
export class LocalDBContactService<T> {
    getAll(): Promise<any[]> {
        return Promise.resolve([]);
    }

    getById(id: string): Promise<any> {
        return Promise.resolve({});
    }

    upserContact(id: string, contact: T): Promise<any> {
        return Promise.resolve({});
    }

    addContacts(contacCollection: T[]): Promise<any> {
        return Promise.resolve(contacCollection);
    }

    updateContacts(contacCollection: T[]): Promise<any> {
        return Promise.resolve(contacCollection);
    }

    deleteteContacts(contacCollection: T[]): Promise<any> {
        return Promise.resolve(contacCollection);
    }
}
