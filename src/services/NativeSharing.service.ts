import { Injectable, Inject } from '@angular/core';
// import { Facebook } from '@ionic-native/facebook';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Platform } from '@ionic/angular';
import { CONTACTS_SERVICE, IContactsService, ContactModel } from '@boxx/contacts-core';
import { Contact } from '@ionic-native/contacts';


@Injectable({
    providedIn: 'root'
})
export class SharingService {

    constructor(
        // private facebook: Facebook,
        @Inject(CONTACTS_SERVICE) private contactSrvc: IContactsService<Contact, ContactModel>,
        private plt: Platform,
        private iab: InAppBrowser,
        private socialSharingSvc: SocialSharing,
    ) {
        this.plt.ready().then(() => {
            this.isIos = this.plt.is('ios');
            // console.warn('IS IOS', this.isIos);
        });
    }

    private isIos = false;

    /*facebookSharing(data: FacebookModule.IPostData): Promise<any> {
        data.method = 'share';
        return this.facebook.showDialog(data)
    }*/

    socialSharing(options: ISharingOptions) {
        return this.socialSharingSvc.shareWithOptions(options);
    }


    whatsappShare(options: IWhatsAppSharingOptions) {
        const message = options.message || null;
        const file = options.image || null;
        const url = options.url || null;
        return this.socialSharingSvc.shareViaWhatsApp(message, file, url);
    }

    whatsappShareToContact(receiver: string, options: IWhatsAppSharingOptions) {
        console.log('WHATSAPP TO', receiver);

        const message = options.message || null;
        const file = options.image || null;
        const url = options.url || null;
        return this.socialSharingSvc.shareViaWhatsAppToReceiver(receiver, message, file, url);
    }

    whatsAppSendMessage(phoneNumber: string, countryCode: string, phoneCode: string, message: string) {
        return new Promise((resolve, reject) => {

            let code: string;
            switch (countryCode) {
                case 'MEX':
                    code = `${phoneCode}1`;
                    break;
                case 'ARG':
                    code = `${phoneCode}9`;
                    break;
                default:
                    code = phoneCode;
            }

            if (this.isIos) {
                const subs = this.contactSrvc.pickOne(phoneNumber).subscribe(contact => {
                    if (!contact) {
                        if (code.startsWith('+')) {
                            code = code.substring(1);
                        }

                        this.openURL(`https://wa.me/${code}${phoneNumber}?text=${encodeURIComponent(message)}`);
                        resolve(contact);
                        return subs.unsubscribe();
                    }

                    this.socialSharingSvc.canShareVia('whatsapp', message).then(() => {
                        this.whatsappShareToContact(contact.id, { message })
                            .then(() => resolve(contact), error => {
                                console.error('**** whatsappShareToContact', error);
                                reject({ after: 'unknown', error: `Whatsapp: ${error}` });
                            })
                            .finally(() => subs.unsubscribe());
                    }, error => {
                        subs.unsubscribe();
                        reject({ after: 'unknown', error: `Whatsapp: ${error}` });
                    });
                }, error => {
                    console.error('**** whatsappShareToContact', error);
                    reject({ after: 'unknown', error: `Whatsapp: ${error}` });
                });
            }
            else {
                this.whatsappShareToContact(`${code}${phoneNumber}`, { message })
                    .then(
                        () => resolve(phoneNumber),
                        error => {
                            console.error('**** whatsappShareToContact', error);
                            reject({ after: 'unknown', error: `Whatsapp: ${error}` });
                        }
                    );
            }
        });
    }

    makeCall(phoneNumber: string) {
        phoneNumber = encodeURIComponent(phoneNumber);
        window.location.href = 'tel:' + phoneNumber;
    }

    sendEmail(email: string) {
        window.location.href = 'mailto:' + email;
    }

    openURL(url: string) {
        this.iab.create(url, '_blank', 'location=yes, clearsessioncache=yes');
    }

}

interface IWhatsAppSharingOptions {
    message: string;
    image?: string;
    url?: string;
}

interface ISharingOptions {
    message?: string;
    subject?: string;
    files?: string | string[];
    url?: string;
    chooserTitle?: string;
}
