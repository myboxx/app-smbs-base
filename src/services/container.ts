import { Provider } from '@angular/core';
import { ContactsRepository, CONTACTS_SERVICE, ContactsService, CONTACTS_REPOSITORY } from '@boxx/contacts-core';
import { MESSAGES_SERVICE, MessagesService, MessagesRepository, MESSAGES_REPOSITORY } from '@boxx/messages-core';
import { EVENTS_SERVICE, EventsService, EventsRepository, EVENTS_REPOSITORY } from '@boxx/events-core';
import { WEBSITE_SERVICE, WebsiteService, WebsiteRepository, WEBSITE_REPOSITORY } from '@boxx/website-core';
import { LoginRepository, LoginService, LOGIN_REPOSITORY, LOGIN_SERVICE } from '@boxx/login-core';
import { PanelRepository, PanelService, PANEL_REPOSITORY, PANEL_SERVICE } from '@boxx/panel-core';
import { ProfileRepository, ProfileService, PROFILE_REPOSITORY, PROFILE_SERVICE } from '@boxx/profile-core';

export const containerService: Provider[] = [
    { provide: PANEL_SERVICE, useClass: PanelService },
    { provide: PANEL_REPOSITORY, useClass: PanelRepository },

    { provide: WEBSITE_SERVICE, useClass: WebsiteService },
    { provide: WEBSITE_REPOSITORY, useClass: WebsiteRepository },

    { provide: CONTACTS_SERVICE, useClass: ContactsService },
    { provide: CONTACTS_REPOSITORY, useClass: ContactsRepository },

    { provide: PROFILE_SERVICE, useClass: ProfileService },
    { provide: PROFILE_REPOSITORY, useClass: ProfileRepository },

    { provide: EVENTS_SERVICE, useClass: EventsService },
    { provide: EVENTS_REPOSITORY, useClass: EventsRepository },

    { provide: LOGIN_SERVICE, useClass: LoginService },
    { provide: LOGIN_REPOSITORY, useClass: LoginRepository },

    { provide: MESSAGES_SERVICE, useClass: MessagesService },
    { provide: MESSAGES_REPOSITORY, useClass: MessagesRepository },
];
