import { NgModule } from '@angular/core';
import { Camera } from '@ionic-native/camera/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';


@NgModule({
    imports: [],
    providers: [
        Camera,
        Crop,
        File,
        FileTransfer,
        ImagePicker,
        InAppBrowser,
        SocialSharing,
    ]
})

export class ServicesModule { }
