import { NgModule } from '@angular/core';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';


export function translateLoaderFactory(theHttpClient: HttpClient) {
    return new TranslateHttpLoader(theHttpClient, 'assets/i18n/', '.json');
}


@NgModule({
    imports: [
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: translateLoaderFactory,
                deps: [HttpClient]
            }
        })
    ],
    providers: [
    ],
    exports: [
        // ...MULTILANG_COMPONENTS,
        TranslateModule
    ]
})
export class MultilingualModule { }
